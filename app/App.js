import React, { Component } from 'react';

//router
import { BrowserRouter as Router } from 'react-router-dom';

//drag'n'drop
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import { default as TouchBackend } from 'react-dnd-touch-backend';

//utils, data
import { supportsTouch } from './exercises/helpers';

//components
import Routes from './routes';
import TokenExpiration from './user/containers/tokenExpiration';
import Overlay from './common/components/Overlay';

const touchDevice = supportsTouch();


@DragDropContext(touchDevice ? TouchBackend : HTML5Backend)
class App extends Component {
  componentWillMount() {
    if (touchDevice) {
      window.addEventListener('click', () => {
        console.log('warning! click with touch interface enabled');
      });
    }
  }
  render() {
    return (
      <Router>
        <div>
          <Routes />
          <TokenExpiration />
          <Overlay />
        </div>
      </Router>
    );
  }
}


export default App;

