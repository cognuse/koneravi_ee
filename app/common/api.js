import axios from 'axios';
import { logout } from '../user/state/actions';

axios.interceptors.request.use((config) => config, (error) => Promise.reject(error));

axios.interceptors.response.use(
  (response) => {
    if (response.config.url === '') {
      if (!response.data.result) {
        logout();
      }
    }
    return response;
  },
  (error) => {
    if (error.status === 401) {
      logout();
    }
    return Promise.reject(error);
  });
//rest
export const LOGIN_URL = '';
export const EXPIRATION_URL = '';
export const REFRESH_URL = '';
export const PATIENTS_URL = '';
export const REPORT_RESULTS_URL = '';
export const RESULTS_URL = '';

export const AUDIOS_URL = '';

//graphql
export const GRAPHQL_URL = '';
export const GRAPHQL_URL_RESULTS = '';

export function apiConfig(token) {
  return (
    {
      headers: { Authorization: `Bearer ${token}` }
    }
  );
}
