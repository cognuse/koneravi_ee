import React, { Component } from 'react';

//router
import { Link } from 'react-router-dom';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../dictionary/global';

//components
import InfoPopup from '../InfoPopup';

//styles, assets
import headerLogo from '../../../../images/konravi_logo_white.png';
import styles from './headerTemplate.css';

class HeaderTemplate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showTemplates: false,
      showPrompt: false
    };

    this.toggleShowPrompt = this.toggleShowPrompt.bind(this);
  }

  componentWillMount() {
    const { userRole } = this.props;
    this.setState({ showTemplates: userRole > 1 });
  }

  toggleShowPrompt() {
    this.setState({ showPrompt: !this.state.showPrompt });
  }

  render() {
    const { title, handleBack, userId, logout, userRole, token } = this.props;
    const { formatMessage } = this.props.intl;
    const { showTemplates, showPrompt } = this.state;
    const buttonClasses = 'mdl-button mdl-layout__tab';
    const activeExerciseClass = title.toLowerCase() === formatMessage(globalMessages.exercises).toLowerCase() ? 'is-active' : '';
    const activeTemplateClass = title.toLowerCase() === formatMessage(globalMessages.templates).toLowerCase() ? 'is-active' : '';

    return (
      <header className={`mdl-layout__header ${styles.header}`}>
        <div className={`mdl-layout__header-row ${styles.headerRow}`}>
          <div className={`mdl-layout__tab ${styles.headerLogo}`}>
            <img src={headerLogo} alt="" className={`${styles.headerLogoImage}`} />
          </div>
          <nav className="mdl-navigation">
            <Link to="/" className={`${buttonClasses} ${styles.headerNavLinks}`}>
              {formatMessage(globalMessages.home)}
            </Link>
            <div className={`${buttonClasses} ${styles.headerNavLinks} ${activeExerciseClass}`} >
              { formatMessage(globalMessages.exercises) }
            </div>
            {userRole >= 10 &&
              <a href={`http://cpms.cognuse.com/login/${token}$$templates`} className={`${buttonClasses} ${styles.headerNavLinks}`}>
                { formatMessage(globalMessages.create) }
              </a>
            }
          </nav>
          <div className="mdl-layout-spacer" />
          <nav
            className={`mdl-navigation ${styles.headerSaveButton}`}
            style={{ marginRight: '44px' }}
          >
            <div
              className={`${buttonClasses} ${styles.headerNavLinks} ${styles.headerLogout}`}
              onClick={logout}
            >
              { formatMessage(globalMessages.logout) }
            </div>
          </nav>
        </div>
        {showPrompt &&
          <InfoPopup
            modalClassName={styles.modal}
            overlayClassName={styles.overlay}
            isOpen={showPrompt}
            onConfirm={this.toggleShowPrompt}
            contentLabel="results"
          >
            {formatMessage(globalMessages.underDevelopment)}
          </InfoPopup>
        }
      </header>
    );
  }
}

export default injectIntl(HeaderTemplate);
