import React, { Component } from 'react';
import Modal from 'react-modal';
import PropTypes from 'prop-types';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';

//styles
import styles from './infoPopup.css';

class InfoPopup extends Component {
  static PropTypes = {
    contentLabel: PropTypes.string.isRequired,
    isOpen: PropTypes.bool.isRequired,
    onConfirm: PropTypes.func.isRequired,
    overlayClassName: PropTypes.string,
    modalClassName: PropTypes.string
  };
  static defaultProps = {
    overlayClassName: '',
    modalClassName: '',
  };

  componentDidMount() {
    window.componentHandler.upgradeDom();
  }

  render() {
    const { overlayClassName, modalClassName, contentLabel, isOpen, onConfirm } = this.props;

    return (
      <Modal
        className={modalClassName}
        overlayClassName={overlayClassName}
        isOpen={isOpen}
        onRequestClose={onConfirm}
        shouldCloseOnOverlayClick
        contentLabel={contentLabel}
      >
        <div className={styles.description}>
          {this.props.children}
        </div>
        <div className={styles.buttons}>
          <button
            className="mdl-button mdl-js-button mdl-button--raised mdl-color--green-500"
            onClick={onConfirm}
          >
            Ok
          </button>
        </div>
      </Modal>
    );
  }
}

export default injectIntl(InfoPopup);
