import React from 'react';
import PropTypes from 'prop-types';

//i18n declaration
import { injectIntl } from 'react-intl';
import { landingNotification } from '../../dictionary/specific';

//styles
import styles from './landingNotification.css';


const LandingNotification = ({ userRole, intl: { formatMessage } }) => {
  const link = userRole === 1 ? 'https://goo.gl/forms/5Z2p8QBnCqsqnc2M2' : 'https://goo.gl/forms/IBV0EVpCdKH5XKk83';
  return (
    <div className={`mdl-shadow--4dp ${styles.wrapper}`}>
      Kliki <a href={link}>siia</a>, kui oled huvitatud videokõne
      funktsionaalsuse arendamisest kõneravi.ee leheküljel
    </div>
  );
};

LandingNotification.propTypes = {
  userRole: PropTypes.number.isRequired
};

export default injectIntl(LandingNotification);
