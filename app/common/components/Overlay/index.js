import React from 'react';
import { connect } from 'react-redux';

//styles
import styles from './overlay.css';


const Overlay = ({ overlay }) => {
  if (overlay === 'spinner') {
    return <div className="global-overlay"><div className={styles.spinner} /></div>;
  }
  return overlay ? <div className="global-overlay" /> : <div />;
};

function mapStateToProps(state) {
  return {
    overlay: state.Exercises.overlay
  };
}

export default connect(mapStateToProps)(Overlay);
