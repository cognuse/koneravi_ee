import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-modal';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';

//styles
import styles from './promptPopup.css';

class PromptPopup extends Component {
  static PropTypes = {
    contentLabel: PropTypes.string.isRequired,
    isOpen: PropTypes.bool.isRequired,
    onRequestClose: PropTypes.func.isRequired,
    onConfirm: PropTypes.func.isRequired,
    promptType: PropTypes.string.isRequired,
    overlayClassName: PropTypes.string,
    modalClassName: PropTypes.string
  };
  static defaultProps = {
    overlayClassName: '',
    modalClassName: '',
  };

  componentDidMount() {
    window.componentHandler.upgradeDom();
  }

  render() {
    const {
      overlayClassName,
      modalClassName,
      contentLabel,
      isOpen,
      onRequestClose,
      promptType,
      onConfirm,
      intl: { formatMessage }
    } = this.props;

    return (
      <Modal
        className={modalClassName}
        overlayClassName={overlayClassName}
        isOpen={isOpen}
        onRequestClose={onRequestClose}
        shouldCloseOnOverlayClick
        contentLabel={contentLabel}
      >

        <div className={styles.description}>
          {this.props.children}
        </div>
        <div className={styles.buttons}>
          <div className={styles.buttonsLeft}>
            <button
              className="mdl-button mdl-js-button mdl-button--raised mdl-color--green-500"
              onClick={() => {
                onConfirm(promptType);
              }}
            >
              {formatMessage(globalMessages.yes)}
            </button>
          </div>
          <div className={styles.buttonsRight}>
            <button
              className="mdl-button mdl-js-button mdl-button--raised"
              onClick={onRequestClose}
            >
              {formatMessage(globalMessages.no)}
            </button>
          </div>
        </div>
      </Modal>
    );
  }
}

export default injectIntl(PromptPopup);
