import React, { Component } from 'react';
import PropTypes from 'prop-types';

//styles
import styles from './pureSelect.css';


class PureSelect extends Component {
  static propTypes = {
    label: PropTypes.string,
    options: PropTypes.array.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      value: props.title
    };

    this.handleOnChange = this.handleOnChange.bind(this);
  }

  componentDidMount() {
    componentHandler.upgradeDom();
  }

  handleOnChange(e) {
    const value = e.target.value;
    const { index, changeSetting, options, label } = this.props;
    changeSetting(index, { value, options, label });
  }

  render() {
    const { label, options, value } = this.props;

    return (
      <div className={styles.pureSelect}>
        <span className={styles.label}>{label}</span>
        <select className={styles.field} value={value} onChange={this.handleOnChange}>
          {options.map((item, index) =>
            <option key={index} value={item}>
              {item}
            </option>
          )}
        </select>
      </div>
    );
  }
}

export default PureSelect;
