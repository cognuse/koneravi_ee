import React, { Component } from 'react';
import PropTypes from 'prop-types';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../dictionary/global';

//styles
import styles from './timer.css';


class Timer extends Component {
  static propTypes = {
    endtime: PropTypes.number.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {
      minutes: '',
      seconds: '',
      ended: false
    };

    this.getTimeRemaining = this.getTimeRemaining.bind(this);
    this.stopTimer = this.stopTimer.bind(this);
  }

  componentDidMount() {
    const { endtime } = this.props;
    this.initializeClock(Date.parse(new Date()) + (endtime * 60 * 1000));
  }

  componentWillReceiveProps(props) {
    if (this.props.endtime !== props.endtime) {
      this.stopTimer();
      this.initializeClock(Date.parse(new Date()) + (props.endtime * 60 * 1000));
    }
  }

  componentWillUnmount() {
    this.stopTimer();
  }

  getTimeRemaining(endtime) {
    const t = endtime - new Date();
    const seconds = Math.floor((t / 1000) % 60);
    const minutes = Math.floor((t / 1000 / 60) % 60);
    return {
      minutes,
      seconds,
      total: t
    };
  }

  updateClock(endtime) {
    const t = this.getTimeRemaining(endtime);
    this.setState({
      minutes: ('0' + t.minutes).slice(-2),
      seconds: ('0' + t.seconds).slice(-2)
    });

    if (t.total <= 0) {
      this.setState({ ended: true });
    }
  }

  initializeClock = (endtime) => {
    this.updateClock(endtime);
    this.timeinterval = setInterval(this.updateClock.bind(this, endtime), 1000);
  };

  stopTimer() {
    clearInterval(this.timeinterval);
  }

  render() {
    const { minutes, seconds, ended } = this.state;
    const { endtime } = this.props;
    const { formatMessage } = this.props.intl;

    if (typeof endtime !== 'number' || Number.isNaN(endtime)) {
      return null;
    }
    if (ended) {
      return <span className={styles.finished}>{formatMessage(globalMessages.goodJob)} <i className="material-icons">mood</i></span>;
    }

    return (
      <span className={styles.timer}>
        <span>{minutes} {formatMessage(globalMessages.minutes)} </span>
        <span>{seconds} {formatMessage(globalMessages.seconds)} </span>
      </span>
    );
  }
}

export default injectIntl(Timer);
