import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Spinner extends Component {
  static propTypes = {
    spinnerClass: PropTypes.string
  };

  componentDidMount() {
    window.componentHandler.upgradeDom();
  }

  render() {
    const { spinnerClass } = this.props;

    const defaultStyles = spinnerClass ?
      {} :
      {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
      };

    return (
      <div className={`spinner-wrapper ${spinnerClass}`} style={defaultStyles}>
        <div className="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active" />
      </div>
    );
  }
}

export default Spinner;
