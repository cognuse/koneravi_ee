export const getQuery = (id, query, group = '') => {
  switch (query) {
    case 'exerciseGrid':
      return exerciseGrid;
    case 'exerciseGroups':
      return Object.assign({}, { query: exerciseGroups.query.replace('exerciseId', id) });
    case 'exercisePreview':
      return Object.assign({}, { query: exercisePreview.query.replace('exerciseId', id) });
    case 'exercise1':
      return Object.assign({}, { query: exerciseOne.query.replace('exerciseId', id) });
    case 'exercise1Views':
      return Object.assign({}, { query: exerciseOneViews.query.replace('exerciseId', id).replace('groupId', group) });
    case 'exercise3':
      return Object.assign({}, { query: exerciseThree.query.replace('exerciseId', id) });
    case 'exercise3Views':
      return Object.assign({}, { query: exerciseThreeViews.query.replace('exerciseId', id).replace('groupId', group) });
    case 'exercise5':
      return Object.assign({}, { query: exerciseFive.query.replace('exerciseId', id) });
    case 'exercise5a':
      return Object.assign({}, { query: exerciseFiveA.query.replace('exerciseId', id) });
    case 'exercise5Views':
    case 'exercise5AViews':
      return Object.assign({}, { query: exerciseFiveViews.query.replace('exerciseId', id).replace('groupId', group) });
    case 'exercise6':
      return Object.assign({}, { query: exerciseSix.query.replace('exerciseId', id) });
    case 'exercise6Views':
      return Object.assign({}, { query: exerciseSixViews.query.replace('exerciseId', id).replace('groupId', group) });
    case 'exercise7':
    case 'exercise7a':
      return Object.assign({}, { query: exerciseSeven.query.replace('exerciseId', id) });
    case 'exercise7Views':
      return Object.assign({}, { query: exerciseSevenViews.query.replace('exerciseId', id).replace('groupId', group) });
    case 'exercise7aViews':
      return Object.assign({}, { query: exerciseSevenAViews.query.replace('exerciseId', id).replace('groupId', group) });
    case 'exercise8':
      return Object.assign({}, { query: exerciseEight.query.replace('exerciseId', id) });
    case 'exercise8Views':
      return Object.assign({}, { query: exerciseEightViews.query.replace('exerciseId', id).replace('groupId', group) });
    case 'exercise9':
      return Object.assign({}, { query: exerciseNine.query.replace('exerciseId', id) });
    case 'exercise9Views':
      return Object.assign({}, { query: exerciseNineViews.query.replace('exerciseId', id).replace('groupId', group) });
    default:
      return 'query does not exist';
  }
};

export function getContentQuery(locale) {
  const topicListEn = ['numbers', 'days', 'months', 'seasons'];
  const topicListEt = ['kuud', 'numbrid', 'paevad', 'varvid', 'aastaajad'];
  switch (locale) {
    case 'et':
      return topicListEt.map(el => (
        Object.assign({}, { query: contentQuery.query.replace('contentTopic', el).replace('contentLocale', 'et') })
      ));
    default:
      return topicListEn.map(el => (
        Object.assign({}, { query: contentQuery.query.replace('contentTopic', el).replace('contentLocale', 'en') })
      ));
  }
}

export function getThankYouQuery(createdBy, exercise) {
  return Object.assign({}, thankYouQuery, { variables: { createdBy, exercise } });
}

export function getExerciseStatisticsQuery(createdBy, exercise) {
  return Object.assign({}, exerciseStatisticsQuery, { variables: { createdBy, exercise } });
}

export function getMutation(query) {
  switch (query) {
    case 'addExerciseFeedback':
      return addExerciseFeedback;
    default:
      return;
  }
}

const thankYouQuery = {
  query: `
    query CountResults($createdBy: String!, $exercise: String!) {
      countResults(createdBy:$createdBy, exercise:$exercise){
        count
        timeTotal
      },
      results(createdBy: $createdBy){
        _id
        created
        timeTotal
      }
    }
  `,
  variables: {
    createdBy: '',
    exercise: ''
  }
};

const exerciseStatisticsQuery = {
  query: `
    query AggregateResultsByDate($createdBy: String!, $exercise: String!) {
      aggregateResultsByDate(createdBy:$createdBy, exercise:$exercise){
        created
        count
        correctCount
        timeTotal
      }
    }
  `,
  variables: {
    createdBy: '',
    exercise: ''
  }
};

const contentQuery = {
  query: `
    {
      content(topic:"contentTopic", locale:"contentLocale") {
        order
        topic
        text
        key
        url
        _id
      }
    }
  `
};

const exerciseGrid = {
  query: `
  {
    exercises {
      _id
      template
      createdBy
      settings {
        exerciseName
        type
        languageDomain
      }
    }
  }
  `
};

const exercisePreview = {
  query: `
    {
      exercise(id:"exerciseId") {
        _id
        template
        createdBy
        settings {
          exerciseName
          shortDescription
          usefullness
          benefits
          type
          languageDomain
          exerciseDescription
        }
      }
    }
  `
};

const exerciseThree = {
  query: `
    {
      exercise(id:"exerciseId") {
        _id
        template
        owner
        createdBy
        settings {
          exerciseName
          shortDescription
          shortCommand
          exerciseDescription
          usefullness
          benefits
          languageDomain
          type
          locale
          recommendedLength
          isRandom
          inverted
        }
      }
    }
  `
};

const exerciseThreeViews = {
  query: `
    {
      views(exercise:"exerciseId" group:"groupId") {
        _id
        order
        group
        originalText
        actions
        image
        audio
      }
    }
  `
};

const exerciseOne = {
  query: `
    {
      exercise(id:"exerciseId") {
        _id
        template
        owner
        createdBy
        settings {
          exerciseName
          shortDescription
          shortCommand
          exerciseDescription
          usefullness
          benefits
          languageDomain
          type
          locale
          recommendedLength
          isRandom
          letters
          correctAnswerFormat
        }
      }
    }
  `
};

const exerciseOneViews = {
  query: `
    {
      views(exercise:"exerciseId" group:"groupId") {
        _id
        order
        group
        originalText
        actions
        audio
        audioState
        image
      }
    }
  `
};

const exerciseFive = {
  query: `
    {
      exercise(id:"exerciseId") {
        _id
        template
        owner
        createdBy
        settings {
          exerciseName
          shortDescription
          numberOfAnswers
          delay
          letters
          hideAnswer
          shortCommand
          recommendedLength
          exerciseDescription
          usefullness
          benefits
          languageDomain
          type
          locale
        }
      }
    }
  `
};

const exerciseFiveViews = {
  query: `
    {
      views(exercise:"exerciseId" group:"groupId") {
        _id
        order
        group
        image
        correctAnswer
        incorrectAnswers
        sentence
      }
    }
  `
};

const exerciseFiveA = {
  query: `
    {
      exercise(id:"exerciseId") {
        _id
        template
        owner
        createdBy
        settings {
          exerciseName
          shortDescription
          numberOfAnswers
          delay
          letters
          hideAnswer
          shortCommand
          recommendedLength
          exerciseDescription
          usefullness
          benefits
          languageDomain
          type
          locale
          inverted
        }
      }
    }
  `
};

const exerciseSix = {
  query: `
    {
      exercise(id:"exerciseId") {
        _id
        template
        owner
        createdBy
        settings {
          exerciseName
          shortDescription
          delay
          isValidated
          recommendedLength
          letters
          shortCommand
          exerciseDescription
          usefullness
          benefits
          type
          languageDomain
          locale
        }
      }
    }
  `
};

const exerciseSixViews = {
  query: `
    {
      views(exercise:"exerciseId" group:"groupId") {
        _id      
        order
        group
        image
        originalText
      }
    }
  `
};

const exerciseSeven = {
  query: `
    {
      exercise(id:"exerciseId") {
        _id
        template
        owner
        createdBy
        settings {
          exerciseName
          shortDescription
          delay
          letters
          shortCommand
          isRandom
          isValidated
          recommendedLength
          exerciseDescription
          usefullness
          benefits
          type
          languageDomain
          locale
        }
      }
    }
  `
};

const exerciseSevenViews = {
  query: `
    {
      views(exercise:"exerciseId" group:"groupId") {
        _id      
        order
        group
        image
        sentence
        correctAnswer
      }
    }
  `
};

const exerciseSevenAViews = {
  query: `
    {
      views(exercise:"exerciseId" group:"groupId") {
        _id      
        order
        group
        image
        audio
        correctAnswer
      }
    }
  `
};

const exerciseEight = {
  query: `
    {
      exercise(id:"exerciseId") {
        _id
        template
        owner
        createdBy
        settings {
          exerciseName
          shortDescription
          audioSpeed
          shortCommand
          exerciseDescription
          recommendedLength
          usefullness
          benefits
          numberOfWords
          type
          languageDomain
        }
      }
    }
  `
};

const exerciseEightViews = {
  query: `
    {
      views(exercise:"exerciseId" group:"groupId") {
        _id
        order
        originalText
        topic
        skippedWords
        image
        audio
      }
    }
  `
};

const exerciseNine = {
  query: `
    {
      exercise(id:"exerciseId") {
        _id
        template
        owner
        createdBy
        settings {
          exerciseName
          shortDescription
          delay
          letters
          recommendedLength
          shortCommand
          exerciseDescription
          usefullness
          benefits
          numberOfAnswers
          languageDomain
          type
          locale
        }
      }
    }
  `
};

const exerciseNineViews = {
  query: `
    {
      views(exercise:"exerciseId" group:"groupId") {
        _id
        order
        group
        incorrectAnswerPairs {
          match
          word
        }
      }
    }
  `
};

const exerciseGroups = {
  query: `
    {
      exerciseGroups(id:"exerciseId") {
        title
      }
    }
  `
};

// TODO: transform queries using variables
const payload = {
  query: `
    query ExerciseGroups($id: String!){
      exerciseGroups(id:$id) {
        title
      }
    }
  `,
  variables: {
    id: '58b52d8b4f4c127ad2c21b2c'
  }
};

//mutations
const addExerciseFeedback =
  `
    mutation AddExerciseFeedback($input: ExerciseFeedbackInput) {
      addExerciseFeedback(input: $input) {
        result
      }
    }
  `;

export const saveResultsQuery =
  `
    mutation CreateResult($input: ResultInput) {
      createResult(input: $input) {
        result
        code
        message
      }
    }
  `;
