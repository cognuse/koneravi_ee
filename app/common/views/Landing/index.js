import React, { Component } from 'react';

//redux
import { connect } from 'react-redux';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../dictionary/global';

//utils
import { logout } from '../../../user/state/actions';
import Modal from 'react-modal';

//components
import PatientsView from '../../../patient/views/PatientView';
import InfoPopup from '../../../common/components/InfoPopup';
import LandingNotification from '../../../common/components/LandingNotification';

//styles
import styles from './landing.css';
import headerLogo from '../../../../images/konravi_logo_white.png';

class Landing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openPatientSelect: false,
      showPrompt: false
    };

    this.toggleModal = this.toggleModal.bind(this);
    this.toggleAlert = this.toggleAlert.bind(this);
  }

  toggleModal() {
    this.setState({ openPatientSelect: !this.state.openPatientSelect });
  }

  toggleAlert() {
    this.setState({ showPrompt: !this.state.showPrompt });
  }

  render() {
    const { userRole, token, logout, history: { push }, intl: { formatMessage } } = this.props;

    const { openPatientSelect, showPrompt } = this.state;
    const linkClass = openPatientSelect ? styles.hidden : styles.visible;
    let colClasses = 'mdl-cell mdl-cell--12-col mdl-cell--12-col-tablet mdl-cell--12-col-phone';
    const mainColClasses = 'mdl-cell mdl-cell--12-col mdl-cell--12-col-tablet mdl-cell--4-col-phone';
    if (userRole >= 10) {
      colClasses = 'mdl-cell mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone';
    }

    //external div is required for correct unmounting of the component by react-router
    return (
      <div>
        <div className="mdl-layout mdl-js-layout mdl-layout--fixed-header landing">
          <header className={`mdl-layout__header ${styles.header}`}>
            <div className={`mdl-layout__header-row ${styles.headerRow}`}>
              <div className={`mdl-layout__tab ${styles.headerLogo}`}>
                <img src={headerLogo} alt="" className={`${styles.headerLogoImage}`} />
              </div>
              <div className="mdl-layout-spacer" />
              <nav
                className={`mdl-navigation ${styles.headerSaveButton}`}
                style={{ marginRight: '44px' }}
              >
                <div
                  className={`mdl-button mdl-layout__tab ${styles.headerNavLinks} ${styles.headerLogout}`}
                  onClick={logout}
                >
                  { formatMessage(globalMessages.logout) }
                </div>
              </nav>
            </div>
          </header>
          <LandingNotification userRole={userRole} />
          <main className={`mdl-layout__content ${styles.content}`}>
            <div className={`mdl-grid mdl-grid--no-spacing ${styles.landingContent}`}>
              <div key="one" className={`${colClasses} ${styles.landingLink} ${linkClass}`}>
                <div
                  onClick={() => {
                    if (userRole > 1) {
                      this.setState({ openPatientSelect: true });
                    } else {
                      push('/practice');
                    }
                  }}
                >
                  {formatMessage(globalMessages.practice)}
                </div>
              </div>
              {
                userRole >= 10 &&
                <div className={`${colClasses} ${styles.landingLink} ${linkClass}`}>
                  <a href={`http://cpms.cognuse.com/login/${token}$$templates`}>{formatMessage(globalMessages.create)}</a>
                </div>
              }
            </div>
          </main>
        </div>
        <Modal
          className={styles.modal}
          overlayClassName={styles.overlay}
          isOpen={openPatientSelect}
          onRequestClose={this.toggleModal}
          shouldCloseOnOverlayClick
          contentLabel={'patientSelect'}
        >
          <PatientsView push={push} />
        </Modal>
        {showPrompt &&
          <InfoPopup
            modalClassName={styles.resultsModal}
            overlayClassName={styles.resultsOverlay}
            isOpen={showPrompt}
            onConfirm={this.toggleAlert}
            contentLabel="results"
          >
            {formatMessage(globalMessages.underDevelopment)}
          </InfoPopup>
        }
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    userRole: state.User.currentUser.role,
    token: state.User.currentUser.token
  };
}

export default connect(mapStateToProps, {
  logout
})(injectIntl(Landing));
