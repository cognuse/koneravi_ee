//i18n
import { addLocaleData } from 'react-intl';
import en from 'react-intl/locale-data/en';
import et from 'react-intl/locale-data/et';
import localeData from '../i18n/translations.json';
//utils, data
import base64 from 'base-64';
import { loadState } from './localStorage';

addLocaleData([...en, ...et]);

//comment next 3 lines to test
export const language = getLocale();
export const messages = localeData[language] || localeData.en;

// uncomment to test
// export const language = 'et';
// export const messages = localeData['et'];

function getLocale() {
  const location = window.location.pathname;
  if (location.search('/login/') > -1) {
    const locale = JSON.parse(base64.decode(location.split('.')[1])).claim.locale;
    return locale === 'ee' ? 'et' : locale;
  }
  const state = loadState();
  if (state && state.User) {
    return state.User.currentUser.locale;
  }
  return 'en';
}
