import promise from 'redux-promise';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
import immutableStateInvariantMiddleware from 'redux-immutable-state-invariant';
import throttle from 'lodash/throttle';
import reducers from './rootReducer';
import { INITIAL_STATE as USER_INITIAL_STATE } from './user/state/reducer';
import { INITIAL_STATE as EXERCISES_INITIAL_STATE } from './exercises/state/reducer';
import { resetLastExerciseData } from './exercises/state/actions';
import { loadState, saveState } from './localStorage';
import LogRocket from 'logrocket';


if (process.env.NODE_ENV === 'production') {
  LogRocket.init('6eo7lw/koneraviee');
  console.log('logRocket activated');
}

const persistedState = loadState();
//TODO: substitute with an API call
//temporary system for favourite exercises restoring
let modifiedPersistedState;
if (persistedState && persistedState.Exercises && persistedState.Exercises.favourites) {
  const persistedExercises = Object.assign(
    {},
    persistedState.Exercises,
    { currentFavourites: persistedState.Exercises.favourites }
  );
  modifiedPersistedState = Object.assign({}, persistedState, { Exercises: persistedExercises });
}

const middleware = process.env.NODE_ENV !== 'production' ?
  [thunk, immutableStateInvariantMiddleware(), promise, LogRocket.reduxMiddleware()]
  :
  [thunk, promise, LogRocket.reduxMiddleware()];

const store = createStore(
  reducers,
  modifiedPersistedState,
  compose(
    applyMiddleware(...middleware),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
);

//reset last exercise
if (persistedState && persistedState.Exercises && !persistedState.Exercises.lastExerciseResult) {
  store.dispatch(resetLastExerciseData());
}

//save part of the state to the local storage to retrieve it as initial state
const initialValues = {
  authenticating: USER_INITIAL_STATE.authenticating,
  isAuthenticated: USER_INITIAL_STATE.isAuthenticated,
  showPromptTokenRefresh: USER_INITIAL_STATE.showPromptTokenRefresh
};

store.subscribe(throttle(() => {
  saveState({
    Patient: store.getState().Patient,
    User: Object.assign({}, store.getState().User, initialValues),
    Exercises: Object.assign(
      {},
      EXERCISES_INITIAL_STATE,
      {
        favourites: store.getState().Exercises.favourites,
        lastExerciseDuration: store.getState().Exercises.lastExerciseDuration,
        lastExerciseResult: store.getState().Exercises.lastExerciseResult,
        lastExerciseId: store.getState().Exercises.lastExerciseId
      }
    )
  });
}), 1000);

export default store;
