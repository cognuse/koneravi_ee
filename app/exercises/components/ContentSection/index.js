import React, { Component } from 'react';
import shortid from 'shortid';
import PropTypes from 'prop-types';

//components
import ExerciseCard from '../ExerciseCard';

//styles
import styles from './contentSection.css';


class ContentSection extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    supportingText: PropTypes.string.isRequired,
    exercises: PropTypes.array.isRequired,
    visible: PropTypes.bool.isRequired
  };

  constructor(props) {
    super(props);
    this.renderCards = this.renderCards.bind(this);
  }

  renderCards(item) {
    const { exerciseName, shortDescription } = item.settings;
    const { push } = this.props;

    return (
      <div key={shortid.generate()} className="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--4-col-phone">
        <ExerciseCard
          data={item}
          title={exerciseName || ''}
          description={shortDescription || ''}
          link={`/exercises/${item.template}/${item._id}`}
          template={item.template}
          push={push}
        />
      </div>
    );
  }

  render() {
    const { title, supportingText, exercises, visible } = this.props;
    const style = {
      display: `${visible ? 'flex' : 'none'}`
    };
    if (!exercises.length) {
      return null;
    }
    return (
      <div className={`mdl-grid mdl-shadow--4dp ${styles.contentSection}`} style={style}>
        <div className={`mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet ${styles.supportingText}`}>
          <h2 className={styles.title}>{title}</h2>
          {supportingText}
        </div>
        <div className={`mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-grid mdl-grid--no-spacing ${styles.cardsField}`}>
          {exercises.map(this.renderCards)}
        </div>
      </div>
    );
  }
}

export default ContentSection;
