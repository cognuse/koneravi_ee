import React, { Component } from 'react';

//components
import ExercisePreview from '../../containers/ExercisePreview';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';

//utils, data
import { capitalise } from '../../helpers';

//styles
import styles from './exerciseCard.css';

class ExerciseCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPreview: false
    };

    this.togglePreview = this.togglePreview.bind(this);
  }

  componentDidMount() {
    componentHandler.upgradeDom();
  }

  togglePreview() {
    this.setState({ showPreview: !this.state.showPreview });
  }

  render() {
    const { title, link, template, push, data: { _id: exerciseId } } = this.props;
    const { showPreview } = this.state;
    const { formatMessage } = this.props.intl;
    const imageUrl = `../../images/template_${template}.png`;

    return (
      <div className={styles.card}>
        <div
          className={styles.title}
          style={{ background: `url('${imageUrl}') no-repeat center center / contain` }}
        />
        <div className={`mdl-card__supporting-text ${styles.titleText}`}>
          {capitalise(title)}
        </div>
        <div
          className={`mdl-button mdl-button--raised mdl-button--colored mdl-js-button mdl-js-ripple-effect ${styles.previewButton}`}
          onClick={this.togglePreview}
        >
          {formatMessage(globalMessages.preview)}
        </div>
        {
          showPreview &&
          <ExercisePreview
            isOpen={showPreview}
            onRequestClose={this.togglePreview}
            link={link}
            imageUrl={imageUrl}
            push={push}
            exerciseId={exerciseId}
            title={title}
          />
        }
      </div>
    );
  }
}

export default injectIntl(ExerciseCard);
