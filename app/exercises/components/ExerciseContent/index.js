import React, { Component } from 'react';
import PropTypes from 'prop-types';

//components
import ContentSection from '../ContentSection';
import NoContentSection from '../NoContentSection';

//i18n declaration
import { injectIntl } from 'react-intl';
import { exercisesGridViewMessages } from '../../../common/dictionary/specific';

//styles
import styles from './exerciseContent.css';

class ExercisesContent extends Component {
  static propTypes = {
    exercises: PropTypes.array.isRequired,
    sections: PropTypes.array.isRequired
  };

  constructor(props) {
    super(props);

    this.renderSections = this.renderSections.bind(this);
    this.renderIncreaseSearchNotice = this.renderIncreaseSearchNotice.bind(this);
  }

  renderSections(section, index, sections) {
    const { exercises, push } = this.props;

    const filteredExercises = exercises.filter((exercise) => (
      exercise.settings.type && exercise.settings.type === section.id
    ));

    const visible = sections.every((section) => !section.visible);

    const visibility = {
      exercises: !!filteredExercises.length,
      section: visible || section.visible
    };

    this.visibility = [...this.visibility, visibility];
    return (
      <ContentSection
        key={index}
        title={section.title}
        supportingText={section.supportingText}
        exercises={filteredExercises}
        visible={visible || section.visible}
        push={push}
      />
    );
  }

  renderIncreaseSearchNotice() {
    const { formatMessage } = this.props.intl;
    if (this.visibility.some(el => el.exercises && el.section)) {
      return <span />;
    }
    return (
      <div className={`mdl-shadow--4dp ${styles.increaseSearchCard}`}>
        {formatMessage(exercisesGridViewMessages.increaseSearch)}
      </div>
    );
  }

  render() {
    const { sections, exercises } = this.props;
    this.visibility = [];

    if (!exercises.length) {
      return <NoContentSection />;
    }

    if (exercises[0] === 'error') {
      return (
        <div className={`mdl-cell mdl-cell--12-col ${styles.noContentError}`}>
          Something is wrong. Please contact us cognuse@cognuse.com
        </div>
      );
    }

    return (
      <div>
        {sections.map(this.renderSections)}
        {this.renderIncreaseSearchNotice()}

      </div>
    );
  }
}


export default injectIntl(ExercisesContent);
