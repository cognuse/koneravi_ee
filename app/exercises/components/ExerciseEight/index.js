import React, { Component } from 'react';
import PropTypes from 'prop-types';

//components
import ExerciseHeader from '../ExerciseHeader/index';

// utils data
import listen from '../../../../images/listen.svg';
import speak from '../../../../images/speak.svg';
import { setAudioDelay } from '../../helpers';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';

//styles
import styles from './exerciseEight.css';


class ExerciseEight extends Component {
  static propTypes = {
    playNext: PropTypes.func.isRequired,
    startExercise: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
    updateCurrentSettings: PropTypes.func.isRequired,
    testQuestions: PropTypes.array.isRequired,
    settings: PropTypes.array.isRequired,
    questionImage: PropTypes.string.isRequired,
    originalText: PropTypes.string.isRequired,
    questionTitle: PropTypes.string
  };

  static defaultProps = {
    questionTitle: ''
  };

  constructor(props) {
    super(props);
    this.state = {};

    this.renderQuestions = this.renderQuestions.bind(this);
    this.renderAudio = this.renderAudio.bind(this);
    this.renderImage = this.renderImage.bind(this);
  }

  renderQuestions(testQuestions) {
    return testQuestions.map(({ text }, index) =>
      <button
        id={`exercise-${text.toLowerCase()}-button`}
        className={`mdl-button mdl-js-button mdl-button--raised mdl-button--colored ${styles.button}`}
        key={index}
      >
        {text}
      </button>
    );
  }

  renderAudio(testQuestions, playNext, settings) {
    const nextTestDelay = setAudioDelay(settings[1].value) + 3000;

    return testQuestions.map(({ _id, text, key, url }, index) => {
      return (
      <audio
        className="exercise-audio"
        id={`exercise-${text.toLowerCase()}-audio`}
        preload="none"
        key={_id}
        onEnded={() => setTimeout(playNext.bind(this, index + 1), nextTestDelay)}
      >
        <source
          src={url}
          type="audio/mp3"
        />
      </audio>
      );
    });
  }

  renderImage(questionImage) {
    const { formatMessage } = this.props.intl;

    switch (questionImage.toLowerCase()) {
      case formatMessage(globalMessages.listen).toLowerCase():
        return <img src={listen} className={styles.image} alt="listen" />;
      case formatMessage(globalMessages.repeat).toLowerCase():
        return < img src={speak} className={styles.image} alt="speak" />;
      case formatMessage(globalMessages.listenRepeat).toLowerCase():
        return (
          <div className={styles.innerImageWrapper}>
            <img src={listen} className={styles.image} alt="listen" />
            <img src={speak} className={styles.image} alt="speak" />
          </div>
        );
      default :
        return <div />;
    }
  }

  render() {
    const {
      questionTitle,
      testQuestions,
      questionImage,
      originalText,
      startExercise,
      playNext,
      settings,
      updateCurrentSettings,
      push,
      intl: { formatMessage }
    } = this.props;

      return (
      <div className="mdl-layout mdl-js-layout mdl-layout--fixed-header exercise8-wrapper">
        <ExerciseHeader
          questionTitle={questionTitle}
          settings={settings}
          updateCurrentSettings={updateCurrentSettings}
          push={push}
        />
        <div className={styles.content}>
          <div className={styles.imageWrapper}>
            {this.renderImage(questionImage)}
          </div>
          <div className={styles.answerWrapper}>
            {testQuestions ? this.renderQuestions(testQuestions) : ''}
          </div>
          <div className="audio-wrapper">
            {testQuestions ? this.renderAudio(testQuestions, playNext, settings) : ''}
          </div>
          <p className={styles.originalText}>
            {originalText}
          </p>
        </div>
        <footer className={styles.footer}>
          <button
            id="start-exercise-button"
            className={`mdl-button mdl-js-button mdl-button--raised ${styles.startButton}`}
            onClick={() => {
              if (testQuestions) {
                startExercise();
              }
            }}
          >
            {formatMessage(globalMessages.start)}
          </button>
        </footer>
      </div>
    );
  }
}

export default injectIntl(ExerciseEight);

