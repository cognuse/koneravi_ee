import React, { Component } from 'react';
import PropTypes from 'prop-types';

//components
import { ExerciseQuestion } from '../exerciseQuestion';
import ExerciseHeader from '../ExerciseHeader/';

//utils, data
import shortid from 'shortid';
import { setExerciseDelay, setTextCase } from '../../helpers';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';

//styles
import styles from './exerciseFive.css';


class ExerciseFive extends Component {
  static propTypes = {
    showNextTest: PropTypes.func.isRequired,
    updateCurrentSettings: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
    testQuestions: PropTypes.object.isRequired,
    settings: PropTypes.array.isRequired,
    questionTitle: PropTypes.string,
    questionImage: PropTypes.string,
    questionText: PropTypes.string,
    template: PropTypes.string,
  };
  static defaultProps = {
    questionTitle: '',
    questionImage: '',
    questionText: '',
    template: ''
  };

  constructor(props) {
    super(props);
    this.state = {
      showAnswers: false
    };

    this.renderAnswers = this.renderAnswers.bind(this);
    this.showAnswers = this.showAnswers.bind(this);
    this.validateAnswer = this.validateAnswer.bind(this);
    this.paint = this.paint.bind(this);
    this.hide = this.hide.bind(this);
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
    if (this.props.questionText) {
      this.setState({ showAnswers: true });
    }
  }

  componentWillReceiveProps(props) {
    if (props.questionImage !== this.props.questionImage || props.questionText !== this.props.questionText) {
      this.setState({ showAnswers: false });
    }
    if (props.questionText !== this.props.questionText) {
      setTimeout(this.showAnswers, 500);
    }
  }

  showAnswers() {
    this.setState({ showAnswers: true });
  }

  paint(target, color) {
    target.style.backgroundColor = color;
  }

  hide(target) {
    target.style.visibility = 'hidden';
  }

  validateAnswer(event, answer) {
    const { formatMessage } = this.props.intl;
    const { testQuestions: { correctAnswer }, showNextTest, settings } = this.props;
    const nextTestDelay = setExerciseDelay(settings[1].value);

    if (correctAnswer !== answer) {
      this.paint(event.target, 'red');
      showNextTest(false);
    } else if (settings[2].value.toLowerCase() === formatMessage(globalMessages.yes).toLowerCase()) {
      this.paint(event.target, '#4caf50');
      setTimeout(this.hide.bind(null, event.target), 500);
      setTimeout(() => { showNextTest(true); }, nextTestDelay + 500);
    } else {
      this.paint(event.target, '#4caf50');
      setTimeout(() => { showNextTest(true); }, nextTestDelay);
    }
  }

  renderAnswers({ correctAnswer, testQuestions }, settings) {
    const answersCase = setTextCase(settings[3].value);
    return testQuestions.map((answer) => {
      const correctAnswerClass = answer === correctAnswer ? answer : '';
      return (
        <div
          className={`mdl-button mdl-js-button mdl-button--raised mdl-button--colored ${correctAnswerClass} ${styles.answer}`}
          style={{ textTransform: answersCase }}
          key={shortid.generate()}
          onClick={(e) => this.validateAnswer(e, answer)}
        >
          {answer}
        </div>
      );
    });
  }

  render() {
    const {
      questionTitle,
      questionImage,
      questionText,
      testQuestions,
      settings,
      template,
      updateCurrentSettings,
      push,
      intl: { formatMessage }
    } = this.props;
    const answersFirst = settings[4].value === formatMessage(globalMessages.answersFirst);
    const imageAndText = template === '5b';
    const imageStyles = {
      backgroundSize: 'contain',
      background: `url('${questionImage}') center center / contain no-repeat`
    };

    return (
      <div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
        <ExerciseHeader
          questionTitle={questionTitle}
          settings={settings}
          updateCurrentSettings={updateCurrentSettings}
          push={push}
        />
        <div
          className={styles.content}
          onLoad={this.showAnswers}
          style={{ display: `${this.state.showAnswers ? 'block' : 'none'}` }}
        >
          {answersFirst && !imageAndText &&
            <div className={styles.answerWrapper}>
              {this.renderAnswers(testQuestions, settings)}
            </div>
          }

          {!imageAndText &&
            <ExerciseQuestion
              questionImage={questionImage}
              questionText={questionText}
            />
          }

          {imageAndText &&
            <div className="question-image-text" style={imageStyles}>
              <img className="picture" src={questionImage} alt="" style={{ display: 'none' }} />
            </div>
          }
          {answersFirst && imageAndText &&
            <div className={styles.answerWrapper}>
              {this.renderAnswers(testQuestions, settings)}
            </div>
          }
          {imageAndText &&
            <div className={styles.questionWrapper}>
              <h2>
                {questionText}
              </h2>
            </div>
          }

          {!answersFirst &&
            <div className={styles.answerWrapper}>
              {this.renderAnswers(testQuestions, settings)}
            </div>
          }
        </div>
      </div>
    );
  }
}

export default injectIntl(ExerciseFive);
