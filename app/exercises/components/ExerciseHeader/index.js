import React, { Component } from 'react';
import PropTypes from 'prop-types';

//components
import Timer from '../../../common/components/Timer/index';
import SettingsModal from '../../containers/SettingsModal/index';

//assets
import listenAgain from '../../../../images/listen_again.svg';

//styles
import styles from './exerciseHeader.css';

class ExerciseHeader extends Component {
  static propTypes = {
    settings: PropTypes.array.isRequired,
    updateCurrentSettings: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
    audio: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array
    ]),
    audioState: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.bool,
    ]),
    questionTitle: PropTypes.string,
    instructions: PropTypes.string,
    pictureTitle: PropTypes.string,
    onAudioEnded: PropTypes.func
  };
  static defaultProps = {
    questionTitle: '',
    audio: '',
    instructions: '',
    pictureTitle: ''
  };

  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false,
      showPlayButton: false
    };

    this.closeExercise = this.closeExercise.bind(this);
    this.toggleSettingsModal = this.toggleSettingsModal.bind(this);
    this.playAudio = this.playAudio.bind(this);
    this.renderAudioNodes = this.renderAudioNodes.bind(this);
    this.playMultipleAudio = this.playMultipleAudio.bind(this);
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
  }

  componentWillReceiveProps(props) {
    const { audioState, audio } = this.props;
    if (audio && props.audioState !== audioState && audioState === 'On') {
      setTimeout(this.playAudio, 500);
    }
    if (audio && props.audioState !== audioState && audioState === 'on multiple') {
      setTimeout(this.playMultipleAudio, 500);
      this.setState({ showPlayButton: false });
    }
  }

  closeExercise() {
    this.props.push('/practice');
  }

  playAudio() {
    this.audio.load();
    this.audio.play();
  }

  playMultipleAudio(index = 0, length) {
    if (index === 0) {
      this.setState({ showPlayButton: false });
    }
    if (index === length) {
      this.setState({ showPlayButton: true });
      if (this.props.showImages) {
        this.props.showImages();
      }
    }
    if (this[`audio-${index}`]) {
      this[`audio-${index}`].load();
      this[`audio-${index}`].play();
    }
  }

  toggleSettingsModal() {
    this.setState({ modalIsOpen: !this.state.modalIsOpen });
  }

  renderAudioNodes() {
    const { questionTitle, audio, instructions, pictureTitle, onAudioEnded } = this.props;
    if ((typeof audio).toLowerCase() === 'string' && !audio.length) {
      return (
        <div className="mdl-layout-title">
          {questionTitle}
        </div>
      );
    }
    if ((typeof audio).toLowerCase() === 'string') {
      return (
        <div className="mdl-layout-title">
          <h4 className={styles.title}>
            <img className={styles.audioButton} src={listenAgain} alt="" onClick={this.playAudio} />
            {`${instructions}${pictureTitle ? ':' : ''} ${pictureTitle}`}
          </h4>
          <audio
            preload="none"
            ref={el => {
              this.audio = el;
            }}
            onEnded={() => {
              if (onAudioEnded) {
                onAudioEnded();
              }
            }}
          >
            <source src={audio} type="audio/mp3" />
          </audio>
        </div>
      );
    }

    return (
      <div className="mdl-layout-title">
        <h4 className={styles.title}>
          { this.state.showPlayButton &&
            <img
              className={styles.audioButton}
              src={listenAgain}
              alt=""
              onClick={() => {
                this.playMultipleAudio();
              }}
            />
          }
          {`${instructions}${pictureTitle ? ':' : ''} ${pictureTitle}`}
        </h4>
        {
          audio.map((record, index, audios) =>
            <audio
              className="exercise-audio"
              preload="none"
              key={record.key}
              ref={(el) => {
                this[`audio-${index}`] = el;
              }}
              onEnded={() => setTimeout(this.playMultipleAudio.bind(this, index + 1, audios.length), 1000)}
            >
              <source
                src={record.url}
                type="audio/mp3"
              />
            </audio>
          )
        }
      </div>
    );
  }

  render() {
    const { settings, updateCurrentSettings } = this.props;

    return (
      <header className={`mdl-layout__header ${styles.header}`}>
        <div className={`mdl-layout__header-row ${styles.headerRow}`}>
          <div>
            {settings.length && <Timer endtime={parseInt(settings[0].value, 10)} />}
          </div>
          <div className="mdl-layout-spacer" />
          {this.renderAudioNodes()}
          <div className="mdl-layout-spacer" />
          <nav className="mdl-navigation">
            <i className={`material-icons ${styles.icon}`} onClick={this.toggleSettingsModal}>
              settings
            </i>
            <i className={`material-icons ${styles.icon}`} onClick={this.closeExercise}>
              input
            </i>
          </nav>
        </div>
        <SettingsModal
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.toggleSettingsModal}
          settings={settings}
          updateCurrentSettings={updateCurrentSettings}
          toggleSettingsModal={this.toggleSettingsModal}
        />
      </header>
    );
  }
}

export default ExerciseHeader;
