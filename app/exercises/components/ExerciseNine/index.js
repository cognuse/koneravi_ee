import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DraggableAnswer from '../draggableAnswer';
import DropArea from '../dropArea';
import ExerciseHeader from '../ExerciseHeader/index';
import CustomDragLayer from '../customDragLayer';
import { setTextCase, formatArrOfStr } from '../../helpers';


//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';

//styles
import styles from './exerciseNine.css';


class ExerciseNine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showAnswers: false,
      checked: false,
      valid: null
    };

    this.showAnswers = this.showAnswers.bind(this);
    this.validateAnswers = this.validateAnswers.bind(this);
    this.checkAnswers = this.checkAnswers.bind(this);
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
  }

  componentWillReceiveProps() {
    this.setState({ showAnswers: false, checked: false, valid: null });
  }

  checkAnswers() {
    const { currentResults } = this.props;
    if (currentResults.every((el) => el !== null)) {
      this.validateAnswers();
      this.setState({ checked: true });
    }
  }

  showAnswers() {
    this.setState({ showAnswers: true });
  }

  validateAnswers() {
    const { currentResults, correctAnswers } = this.props;
    this.setState({
      valid: correctAnswers.every((el, index) => el.match === currentResults[index])
    });
  }

  renderReferences(currentReferences) {
    return currentReferences.map((reference, index) =>
      <div key={index} className="reference-wrapper"><h4>{reference}</h4></div>
    );
  }

  renderAnswers(currentAnswers, moveAnswer) {
    return currentAnswers.map((answer, index) =>
      <DraggableAnswer
        moveAnswer={moveAnswer}
        answer={answer}
        index={index}
        key={index}
        restoreState={this.props.restoreState}
        backupState={this.props.backupState}
      />
    );
  }

  renderResults(answersAmount, currentResults, moveAnswer, currentReferences, autoValidation) {
    const { answerPairs } = this.props;
    const { formatMessage } = this.props.intl;
    let dropZone = [];
    for (let i = 0; i < answersAmount; i++) {
      if (currentResults[i]) {
        const isCorrectSlot = answerPairs
          .some((el) => el.match.toLowerCase() === currentResults[i].toLowerCase() && el.word.toLowerCase() === currentReferences[i].toLowerCase());

        let borderColor = {};
        if (autoValidation.toLowerCase() === formatMessage(globalMessages.yes).toLowerCase() || this.state.checked) {
          borderColor = isCorrectSlot ? { borderColor: '#5cb85c' } : { borderColor: 'red' };
        }

        dropZone = dropZone.concat(
          <DropArea
            index={i}
            occupiedWith={currentResults[i]}
            key={i}
            rearrange={this.props.rearrange}
            restoreState={this.props.restoreState}
            whenHaveNested={{ minWidth: '10px' }}
            borderColor={borderColor}
          >
            <DraggableAnswer
              moveAnswer={moveAnswer}
              answer={currentResults[i]}
              index={i}
              key={i}
              restoreState={this.props.restoreState}
              backupState={this.props.backupState}
              whenNested={{ margin: '5px 6px' }}
              result
            />
          </DropArea>
        );
      } else {
        dropZone = dropZone.concat(
          <DropArea
            rearrange={this.props.rearrange}
            index={i}
            key={i}
            restoreState={this.props.restoreState}
          />
        );
      }
    }
    return dropZone;
  }

  renderButton() {
    const { showNextTest } = this.props;
    const { formatMessage } = this.props.intl;
    const { checked, valid } = this.state;
    if (checked) {
      return (
        <button
          className={`mdl-button mdl-js-button mdl-button--raised mdl-button--accent ${styles.button} ${styles.buttonNextAndCheck}`}
          onClick={() => { showNextTest(valid); }}
        >
          {formatMessage(globalMessages.next)}
        </button>
      );
    }
    return (
      <button
        className={`mdl-button mdl-js-button mdl-button--raised mdl-button--accent ${styles.button} ${styles.buttonNextAndCheck}`}
        onClick={this.checkAnswers}
      >
        {formatMessage(globalMessages.check)}
      </button>
    );
  }

  render() {
    const {
      questionTitle,
      moveAnswer,
      currentAnswers,
      currentResults,
      answersAmount,
      currentReferences,
      settings,
      updateCurrentSettings,
      push,
      showNextTest,
      answerPairs,
      intl: { formatMessage }
    } = this.props;

    const answersCase = setTextCase(settings[4].value);
    const formatedCurrentReferences = formatArrOfStr(currentReferences, answersCase);
    const formatedCurrentAnswers = formatArrOfStr(currentAnswers, answersCase);
    const formatedCurrentResults = formatArrOfStr(currentResults, answersCase);
    return (
      <div className="mdl-layout mdl-js-layout mdl-layout--fixed-header">
        <ExerciseHeader
          questionTitle={questionTitle}
          settings={settings}
          updateCurrentSettings={updateCurrentSettings}
          push={push}
        />
        <div className="exercise-content exercise-nine" style={{ textTransform: answersCase }}>
          <div className="answers-wrapper">
            {this.renderReferences(formatedCurrentReferences)}
          </div>
          <div className="answers-wrapper">
            {this.renderResults(answersAmount, formatedCurrentResults, moveAnswer, formatedCurrentReferences, settings[4].value)}
          </div>
          <div className="answers-wrapper">
            {this.renderAnswers(formatedCurrentAnswers, moveAnswer)}
          </div>
          <CustomDragLayer exerciseNumber="nine" />
        </div>
        <div className="exercise-footer">
          <div className={`${styles.answerWrapper} ${styles.wrapNextAndCheck}`}>
            {settings[4].value.toLowerCase() === formatMessage(globalMessages.no).toLowerCase() && this.renderButton(showNextTest)}
          </div>
        </div>
      </div>
    );
  }
}

export default injectIntl(ExerciseNine);
