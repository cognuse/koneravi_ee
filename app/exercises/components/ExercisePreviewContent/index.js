import React, { Component } from 'react';
import PropTypes from 'prop-types';

//router
import { Link } from 'react-router-dom';

//utils, data
import { Scrollbars } from 'react-custom-scrollbars';

//i18n declaration
import { injectIntl } from 'react-intl';
import { exercisesPreview } from '../../../common/dictionary/specific';

//components
import CloseButton from '../../../common/components/closeButton';

//styles
import styles from './exercisePreviewContent.css';


class ExercisePreviewContent extends Component {
  static propTypes = {
    onRequestClose: PropTypes.func.isRequired,
    link: PropTypes.string.isRequired,
    imageUrl: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);

    this.renderButtons = this.renderButtons.bind(this);
  }

  componentDidMount() {
    componentHandler.upgradeDom();
  }

  renderButtons() {
    const { topics, link, selectedTopic, pushToExercise, addToFavourites, toggleContent, toggleTopics, intl: { formatMessage } } = this.props;
    const buttonClasses = 'mdl-button mdl-button--raised mdl-button--colored mdl-js-button mdl-js-ripple-effect ';

    if (topics.length) {
      return (
        <div className={styles.buttonsWrapper}>
          <div
            className={`${buttonClasses} ${styles.selectTopic}`}
            onClick={toggleTopics}
            ref={el => { this.selectTopicButton = el; }}
          >
            {selectedTopic || formatMessage(exercisesPreview.selectTopic)}
          </div>
          <div className={styles.bottomButtonsWrapper}>
            <button
              className={`${buttonClasses} ${styles.beginWhenTopics}`}
              onClick={pushToExercise}
            >
              {formatMessage(exercisesPreview.begin)}
            </button>
            <button
              className={`${buttonClasses} ${styles.beginWhenTopics}`}
              style={{
                margin: '8px 0 0 2%'
              }}
              onClick={toggleContent}
            >
              {formatMessage(exercisesPreview.checkResults)}
            </button>
          </div>
          <div
            className={`${buttonClasses} ${styles.addToFavouritesWhenTopics}`}
            onClick={addToFavourites}
          >
            {formatMessage(exercisesPreview.favourites)}
          </div>
        </div>
      );
    }
    return (
      <div>
        <Link
          className={`${buttonClasses} ${styles.begin}`}
          to={link}
        >
          {formatMessage(exercisesPreview.begin)}
        </Link>
        <div
          className={`${buttonClasses} ${styles.begin}`}
          style={{
            marginTop: '8px'
          }}
          onClick={toggleContent}
        >
          {formatMessage(exercisesPreview.checkResults)}
        </div>
        <div
          className={`${buttonClasses} ${styles.addToFavourites}`}
          onClick={addToFavourites}
        >
          {formatMessage(exercisesPreview.favourites)}
        </div>
      </div>
    );
  }

  render() {
    const { imageUrl, settings, onRequestClose, topics, showTopicsPopup, setTopic, intl: { formatMessage } } = this.props;

    return (
      <div className={`mdl-grid ${styles.preview}`}>
        <CloseButton
          mainClassName={styles.back}
          iconClassName={styles.backIcon}
          text={formatMessage(exercisesPreview.back)}
          handleClose={onRequestClose}
        />
        <div
          className={`mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone ${styles.leftSection}`}>
          <div className={styles.scrollbarsWrapper}>
            <Scrollbars>
              <h2 className={styles.title}>{settings.exerciseName}</h2>
              <div className={styles.instructions}>
                <h5>{formatMessage(exercisesPreview.introduction)}</h5>
                {settings.exerciseDescription}
              </div>
              <div className={styles.benefits}>
                <h5>{formatMessage(exercisesPreview.benefits)}</h5>
                {settings.benefits}
              </div>
              <div className={styles.usefullness}>
                <h5>{formatMessage(exercisesPreview.usefullness)}</h5>
                {settings.usefullness}
              </div>
            </Scrollbars>
          </div>
        </div>
        <div className={`mdl-cell mdl-cell--6-col mdl-cell--4-col-tablet mdl-cell--2-col-phone ${styles.rightSection}`}>
          <div className={styles.scrollbarsWrapper}>
            <div
              className={styles.image}
              style={{ background: `url('${imageUrl}') no-repeat top center / contain` }}
            />
            {this.renderButtons()}
          </div>
        </div>
        {showTopicsPopup &&
        <div className={`mdl-shadow--4dp ${styles.topicsPopup}`}>
          <Scrollbars>
            {
              topics.map((el, index) => (
                <div
                  key={index}
                  className={styles.listItem}
                  onClick={setTopic}
                >
                  {el}
                </div>
              ))
            }
          </Scrollbars>
          <div
            className={styles.triangle}
            style={{ top: `${this.selectTopicButton.offsetTop + (this.selectTopicButton.offsetHeight / 2) - 70}px` }}
          />
        </div>
        }
      </div>
    );
  }
}

export default injectIntl(ExercisePreviewContent);
