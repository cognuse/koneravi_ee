import React, { Component } from 'react';
import PropTypes from 'prop-types';

//components
import { ExerciseQuestion } from '../exerciseQuestion';
import DraggableAnswer from '../draggableAnswer';
import DropArea from '../dropArea';
import ExerciseHeader from '../ExerciseHeader/';
import CustomDragLayer from '../customDragLayer';

//utils, data
import { setTextCase } from '../../helpers';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';

//styles
import styles from './exerciseSix.css';


class ExerciseSix extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showAnswers: false,
      checked: false,
      valid: null
    };

    this.showAnswers = this.showAnswers.bind(this);
    this.validateAnswers = this.validateAnswers.bind(this);
    this.checkAnswers = this.checkAnswers.bind(this);
    this.setHeight = this.setHeight.bind(this);
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
    window.addEventListener('resize', this.setHeight, false);
  }

  componentWillReceiveProps(props) {
    if (props.questionImage !== this.props.questionImage) {
      this.setState({ showAnswers: false, checked: false, valid: null });
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setHeight);
  }

  setHeight() {
    this.setState({ windowHeight: `${window.innerHeight}px` });
  }

  checkAnswers() {
    const { currentResults } = this.props;
    if (currentResults.every((el) => el !== null)) {
      this.validateAnswers();
      this.setState({ checked: true });
    }
  }

  validateAnswers() {
    const { currentResults, currentReferences } = this.props;
    if (currentResults.every((el, index) => el === currentReferences[index])) {
      this.setState({ valid: true });
    } else {
      this.setState({ valid: false });
    }
  }

  showAnswers() {
    this.setState({ showAnswers: true });
  }

  renderAnswers(currentAnswers, moveAnswer) {
    return currentAnswers.map((answer, index) =>
      <DraggableAnswer
        moveAnswer={moveAnswer}
        answer={answer}
        index={index}
        key={index}
        restoreState={this.props.restoreState}
        backupState={this.props.backupState}
      />
    );
  }

  renderResults(answersAmount, currentResults, moveAnswer, currentReferences, autoValidation) {
    const { formatMessage } = this.props.intl;
    let dropZone = [];
    for (let i = 0; i < answersAmount; i++) {
      if (currentResults[i]) {
        let borderColor;
        if (autoValidation.toLowerCase() === formatMessage(globalMessages.yes).toLowerCase() || this.state.checked) {
          borderColor = currentReferences[i] === currentResults[i] ? { borderColor: '#5cb85c' } : { borderColor: 'red' };
        }
        dropZone = dropZone.concat(
          <DropArea
            index={i}
            occupiedWith={currentResults[i]}
            key={i}
            rearrange={this.props.rearrange}
            restoreState={this.props.restoreState}
            whenHaveNested={{ minWidth: '10px' }}
            borderColor={borderColor}
          >
            <DraggableAnswer
              moveAnswer={moveAnswer}
              answer={currentResults[i]}
              index={i}
              key={i}
              restoreState={this.props.restoreState}
              backupState={this.props.backupState}
              whenNested={{ margin: '5px 6px' }}
              result
            />
          </DropArea>
        );
      } else {
        dropZone = dropZone.concat(
          <DropArea
            rearrange={this.props.rearrange}
            index={i}
            key={i}
            restoreState={this.props.restoreState}
          />
        );
      }
    }
    return dropZone;
  }

  renderButton() {
    const { showNextTest } = this.props;
    const { formatMessage } = this.props.intl;
    const { checked, valid } = this.state;
    if (checked) {
      return (
        <button
          className={`mdl-button mdl-js-button mdl-button--raised mdl-button--accent ${styles.button}`}
          onClick={() => { showNextTest(valid); }}
        >
          {formatMessage(globalMessages.next)}
        </button>
      );
    }
    return (
      <button
        className={`mdl-button mdl-js-button mdl-button--raised mdl-button--accent ${styles.button}`}
        onClick={this.checkAnswers}
      >
        {formatMessage(globalMessages.check)}
      </button>
    );
  }

  render() {
    const {
      questionTitle,
      questionImage,
      moveAnswer,
      currentAnswers,
      currentResults,
      currentReferences,
      answersAmount,
      settings,
      updateCurrentSettings,
      showNextTest,
      push,
      intl: { formatMessage }
    } = this.props;
    const answersCase = setTextCase(settings[2].value);

    return (
      <div
        className={`mdl-layout mdl-js-layout mdl-layout--fixed-header ${styles.container}`}
        style={{ height: this.state.windowHeight || `${window.innerHeight}px` }}
      >
        <ExerciseHeader
          questionTitle={questionTitle}
          settings={settings}
          updateCurrentSettings={updateCurrentSettings}
          push={push}
        />
        <div
          className={styles.content}
          onLoad={() => setTimeout(this.showAnswers, 200)}
          style={{ display: `${this.state.showAnswers ? 'block' : 'none'}` }}
        >
          <ExerciseQuestion questionImage={questionImage} />
          <div
            className={styles.answerArea}
            style={{ textTransform: answersCase }}
            ref={(el) => {
              if (el) {
                document.querySelector('.question-image').style.height = `${window.innerHeight - el.offsetHeight - 94}px`;
              }
            }}
          >
            <div className={`${styles.answerWrapper} ${styles.firstRow}`}>
              {this.renderAnswers(currentAnswers, moveAnswer)}
            </div>
            <div className={styles.answerWrapper}>
              {this.renderResults(answersAmount, currentResults, moveAnswer, currentReferences, settings[3].value)}
            </div>
            <div className={`${styles.answerWrapper} ${styles.referenceWrapper}`}>
              {this.state.valid === false && currentReferences.join(' ')}
            </div>
            <div className={styles.answerWrapper}>
              {settings[3].value.toLowerCase() === formatMessage(globalMessages.no).toLowerCase() && this.renderButton(showNextTest)}
            </div>
            <CustomDragLayer />
          </div>
        </div>
      </div>
    );
  }
}

export default injectIntl(ExerciseSix);
