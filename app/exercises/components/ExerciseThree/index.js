import React, { Component } from 'react';
import PropTypes from 'prop-types';

//utils, data
import difference from 'lodash/difference';
import { replaceWithBigger } from '../../helpers';

//components
import ExerciseHeader from '../ExerciseHeader/index';

//styles
import styles from './exerciseThree.css';


class ExerciseThree extends Component {
  static propTypes = {
    testIndex: PropTypes.number.isRequired,
    showNextTest: PropTypes.func.isRequired,
    push: PropTypes.func.isRequired,
    updateCurrentSettings: PropTypes.func.isRequired,
    settings: PropTypes.array.isRequired,
    questionTitle: PropTypes.string,
    questionImage: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array
    ]),
    questionAudio: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.array
    ])
  };
  static defaultProps = {
    questionTitle: '',
    questionImage: '',
    questionAudio: ''
  };

  constructor(props) {
    super(props);
    this.state = {
      startAudio: false,
      answers: []
    };

    this.startAudio = this.startAudio.bind(this);
    this.renderImages = this.renderImages.bind(this);
    this.validateAnswer = this.validateAnswer.bind(this);
    this.validateResult = this.validateResult.bind(this);
  }

  componentWillReceiveProps(props) {
    if (props.testIndex !== this.props.testIndex) {
      this.setState({ startAudio: false });
      this.correct = true;
    }
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
  }

  validateAnswer(target, index) {
    const { answers, startAudio } = this.state;
    const { actions } = this.props;
    if (!startAudio) {
      return;
    }
    const correctResults = Object.keys(actions);
    if (correctResults.indexOf(index.toString()) > -1) {
      target.style.border = '5px solid #5cb85c';
      this.setState({ answers: [...answers, index.toString()] });
      setTimeout(this.validateResult, 1000);
    } else {
      target.style.border = '5px solid red';
      this.validateResult();
      this.correct = false;
    }
  }

  validateResult() {
    const { actions, showNextTest } = this.props;
    const { answers } = this.state;
    const correctResults = Object.keys(actions);
    if (!difference(correctResults, answers).length) {
      showNextTest(this.correct);
    }
  }

  startAudio() {
    this.setState({ startAudio: true });
  }

  renderImages() {
    const { questionImage } = this.props;
    let layoutClass;

    switch (questionImage.length) {
      case 2:
        layoutClass = `${styles.oneRow} ${styles.twoInRow}`;
        break;
      case 4:
        layoutClass = `${styles.twoRows} ${styles.twoInRow}`;
        break;
      case 6:
        layoutClass = `${styles.twoRows} ${styles.threeInRow}`;
        break;
      case 8:
      default:
        layoutClass = `${styles.twoRows} ${styles.fourInRow}`;
    }

    return (
      <div className={styles.layoutWrapper}>
        {
          questionImage.map((image, index) => (
            <div
              className={`${layoutClass} ${styles.imageWrapper}`}
              key={image.key}
              ref={el => this[image.key] = el}
              onClick={() => {
                this.validateAnswer(this[image.key], index);
              }}
            >
              <img
                src={replaceWithBigger(image.url)}
                alt=""
                className={styles.image}
                onLoad={() => {
                  if (index === questionImage.length - 1) {
                    setTimeout(this.startAudio, 1000);
                  }
                }}
              />
            </div>
          ))
        }
      </div>
    );
  }

  render() {
    const { startAudio } = this.state;
    const {
      questionAudio,
      settings,
      instructions,
      updateCurrentSettings,
      showImages,
      push
    } = this.props;
    return (
      <div
        className="mdl-layout mdl-js-layout mdl-layout--fixed-header exercise-one-wrapper"
      >
        <ExerciseHeader
          settings={settings}
          updateCurrentSettings={updateCurrentSettings}
          audio={questionAudio}
          instructions={instructions}
          audioState={!startAudio && 'on multiple'}
          showImages={showImages}
          push={push}
        />
        <div
          className={styles.content}
        >
          {this.renderImages()}
        </div>
      </div>
    );
  }
}

export default ExerciseThree;
