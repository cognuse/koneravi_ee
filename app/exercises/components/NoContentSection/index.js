import React, { Component } from 'react';
import PropTypes from 'prop-types';

//i18n declaration
import { injectIntl } from 'react-intl';
import { noContentSection } from '../../../common/dictionary/specific';

//styles
import styles from './noContentSection.css';


class NoContentSection extends Component {
  static propTypes = {
    // title: PropTypes.string.isRequired,
    // supportingText: PropTypes.string.isRequired,
    // exercises: PropTypes.array.isRequired,
    // visible: PropTypes.bool.isRequired
  };

  render() {
    const { formatMessage } = this.props.intl;

    return (
      <div className={`mdl-cell mdl-cell--12-col mdl-shadow--4dp ${styles.contentSection}`}>
        <h3 className={styles.title}>{formatMessage(noContentSection.title)}</h3>
        <p className={`${styles.supportingText}`}>
          {formatMessage(noContentSection.subTitle)}
        </p>
        <p>
          {formatMessage(noContentSection.step1)}
        </p>
        <p>
          {formatMessage(noContentSection.step2)}
        </p>
        <p>
          {formatMessage(noContentSection.step3)}
        </p>
        <p>
          {formatMessage(noContentSection.step4)}
        </p>
      </div>
    );
  }
}

export default injectIntl(NoContentSection);
