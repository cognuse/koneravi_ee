import React, { Component } from 'react';
import PropTypes from 'prop-types';

//router
import { Link } from 'react-router-dom';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';
import { warnings } from '../../../common/dictionary/specific';

//styles
import styles from './warningPopup.css';


class WarningPopup extends Component {
  static propTypes = {
    text: PropTypes.string
  };

  render() {
    const { text, intl: { formatMessage } } = this.props;

    return (
      <div>
        <div className={styles.overlay}>
          <div className={`mdl-card mdl-shadow--4dp ${styles.demoCard}`}>
            <div className={styles.title}>
              <h2 className={styles.titleText}>{formatMessage(warnings.attention)}</h2>
            </div>
            <div className={styles.supportingTextWrapper}>
              <div className={styles.supportingText}>
                {text}
              </div>
            </div>
            <div className={styles.buttonWrapper}>
              <Link to="/">
                <button className={`mdl-button mdl-js-button mdl-button--raised mdl-button--colored ${styles.button}`}>
                  {formatMessage(globalMessages.back)}
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


export default injectIntl(WarningPopup);
