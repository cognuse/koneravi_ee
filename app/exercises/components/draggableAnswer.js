import React, { Component } from 'react';
import { DragSource } from 'react-dnd';
import PropTypes from 'prop-types';


const boxSource = {
  beginDrag(props) {
    props.backupState();
    return {
      index: props.index,
      answer: props.answer,
      inResult: props.result
    }; // monitor.getItem()
  },
  endDrag(props, monitor) {
    const item = monitor.getItem();
    const dropResult = monitor.getDropResult();
    if (dropResult) {
      props.moveAnswer(item.index, dropResult.index, item.answer, item.inResult, dropResult.occupiedWith);
    } else {
      props.restoreState();
    }
  }
};

@DragSource('answer', boxSource, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging()
}))
export default class DraggableAnswer extends Component {
  static propTypes = {
    connectDragSource: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired
  };

  componentDidMount() {
    window.componentHandler.upgradeDom();
  }

  render() {
    const { isDragging, connectDragSource, answer, whenNested } = this.props;
    const opacity = isDragging ? 0.4 : 1;

    return (
      connectDragSource(
        <div className="button-wrapper">
          <div
            className={'mdl-button mdl-js-button mdl-button--raised mdl-button--colored '}
            style={Object.assign({}, { opacity }, whenNested, { textTransform: 'inherit' })}
          >
            {answer}
          </div>
        </div>
      )
    );
  }
}
