import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { DropTarget } from 'react-dnd';


const boxTarget = {
  drop(props) {
    return {
      index: props.index,
      occupiedWith: props.occupiedWith
    }; // monitor.getDropResult()
  },
  hover(props, monitor, component) {

    if (monitor.getItem().answer === component.props.occupiedWith
      && monitor.getItem().index === component.props.index
      && monitor.getItem().inResult) {
      return;
    }

    props.rearrange(props.index, props.occupiedWith, monitor.getItem());
  }
};

@DropTarget('answer', boxTarget, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver(),
  canDrop: monitor.canDrop()
}))
export default class DropArea extends Component {
  static propTypes = {
    connectDropTarget: PropTypes.func.isRequired,
    isOver: PropTypes.bool.isRequired,
    canDrop: PropTypes.bool.isRequired
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.isOver !== this.props.isOver && this.props.isOver && nextProps.canDrop === this.props.canDrop) {
      this.props.restoreState();
    }
  }

  render() {
    const { canDrop, isOver, connectDropTarget, borderColor } = this.props;
    const isActive = canDrop && isOver;
    let backgroundClass = '';
    if (isActive) {
      backgroundClass = 'active';
    } else if (canDrop) {
      backgroundClass = 'drop-ready';
    }
    return connectDropTarget(
      <div className="drop-area-wrapper">
        <div
          className={`drop-area ${backgroundClass}`}
          style={Object.assign({}, this.props.whenHaveNested, borderColor || {})}
        >
          {this.props.children}
        </div>
      </div>
    );
  }
}
