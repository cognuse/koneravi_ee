import React from 'react';

export const ExerciseQuestion = ({ questionImage, questionText, handleLoad }) => {
  return (
    questionImage ?
      <div
        className="question-image"
        style={{
          backgroundSize: 'contain',
          background: `url('${questionImage}') center center / contain no-repeat`
        }}
      >
        <img
          className="picture"
          style={{ display: 'none' }}
          src={questionImage}
          onLoad={handleLoad}
          alt=""
        />
      </div> :
      <div className="question-wrapper">
        <h2>
          {questionText}
        </h2>
      </div>
  );
};
