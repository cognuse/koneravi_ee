import React, { Component } from 'react';
import PropTypes from 'prop-types';

//utils, data
import { getQuery, getExerciseStatisticsQuery } from '../../../common/queries';

//redux
import { connect } from 'react-redux';
import {
  getExercisePreview,
  resetCurrentExercise,
  addToFavourites,
  setSelectedTopic,
  getExerciseStatistics
} from '../../state/actions';

//components
import Modal from 'react-modal';
import Spinner from '../../../common/components/spinner';
import PreviewContent from '../../components/ExercisePreviewContent';
import PreviewChart from '../../components/ExercisePreviewChart';

//styles
import styles from './exercisePreview.css';


class ExercisePreview extends Component {
  static propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onRequestClose: PropTypes.func.isRequired,
    link: PropTypes.string.isRequired,
    imageUrl: PropTypes.string.isRequired,
    title: PropTypes.string
  };

  static defaultProps = {
    title: ''
  };

  constructor(props) {
    super(props);
    this.state = {
      showTopicsPopup: false,
      pushToExercise: false,
      showChart: false,
      config: null
    };

    this.addToFavourites = this.addToFavourites.bind(this);
    this.toggleTopics = this.toggleTopics.bind(this);
    this.toggleContent = this.toggleContent.bind(this);
    this.setTopic = this.setTopic.bind(this);
    this.pushToExercise = this.pushToExercise.bind(this);
  }

  componentWillMount() {
    const { link, getExercisePreview, getExerciseStatistics, patientId, userId, exerciseId, locale, title } = this.props;
    const id = link.match(/[^\/]*$/)[0];

    getExercisePreview(getQuery(id, 'exercisePreview'), getQuery(id, 'exerciseGroups'));
    getExerciseStatistics(getExerciseStatisticsQuery(patientId || userId, exerciseId))
      .then((series) => {
        this.setState({ config: Object.assign({}, getChartConfig(locale, title), { series }) });
      });
  }

  componentWillUnmount() {
    if (!this.state.pushToExercise) {
      this.props.resetCurrentExercise();
    }
  }

  addToFavourites() {
    const { addToFavourites, currentExercise } = this.props;
    addToFavourites(currentExercise);
  }

  toggleTopics() {
    this.setState({ showTopicsPopup: !this.state.showTopicsPopup });
  }

  toggleContent() {
    this.setState({ showChart: !this.state.showChart });
  }

  setTopic(e) {
    this.toggleTopics();
    const selectedTopic = e.target.textContent;
    this.props.setSelectedTopic(selectedTopic);
  }

  pushToExercise() {
    const { link, push } = this.props;
    this.setState({ pushToExercise: true }, () => {
      push(link);
    });
  }

  renderContent() {
    const { showChart, showTopicsPopup } = this.state;
    const { config } = this.state;

    return showChart ?
      <PreviewChart
        toggleContent={this.toggleContent}
        config={config}
      />
      :
      <PreviewContent
        showTopicsPopup={showTopicsPopup}
        toggleTopics={this.toggleTopics}
        setTopic={this.setTopic}
        addToFavourites={this.addToFavourites}
        toggleContent={this.toggleContent}
        pushToExercise={this.pushToExercise}
        {...this.props}
      />;
  }

  render() {
    const { isOpen, settings, onRequestClose } = this.props;

    return (
      <Modal
        className="preview-modal mdl-shadow--4dp"
        overlayClassName="settings-modal-overlay"
        isOpen={isOpen}
        onRequestClose={onRequestClose}
        shouldCloseOnOverlayClick
        contentLabel="Exercise preview"
      >
        {settings ?
          this.renderContent()
          :
          <Spinner spinnerClass={styles.spinnerWrapper} />
        }
      </Modal>
    );
  }
}


function mapStateToProps(state) {
  return {
    currentExercise: state.Exercises.currentExercise,
    settings: state.Exercises.currentExercise.settings,
    selectedTopic: state.Exercises.selectedTopic,
    topics: state.Exercises.topics,
    userId: state.User.currentUser.id,
    locale: state.User.currentUser.locale,
    patientId: state.Patient.patientId
  };
}


export default connect(mapStateToProps, {
  getExercisePreview,
  resetCurrentExercise,
  addToFavourites,
  setSelectedTopic,
  getExerciseStatistics
})(ExercisePreview);

const getChartConfig = (locale, title) => ({
  chart: {
    height: 600
  },
  rangeSelector: {
    allButtonsEnabled: true,
    buttons: [
      {
        count: 1,
        type: 'month',
        text: '1k'
      },
      {
        count: 3,
        type: 'month',
        text: '3k'
      },
      {
        type: 'year',
        count: 1,
        text: '1a'
      },
      {
        type: 'all',
        text: 'Kõik'
      }
    ],
    buttonTheme: {
      width: 60
    },
    selected: 3
  },
  subtitle: {
    text: locale === 'et' ? 'TULEMUSED ' : 'RESULTS',
    style: {
      fontSize: '14px'
    },
    y: 25
  },
  title: {
    text: title,
    margin: 70,
    y: 70,
    style: {
      fontSize: '18px'
    }
  },
  legend: {
    enabled: true,
    itemHoverStyle: {
      cursor: 'default'
    }
  },
  navigation: {
    buttonOptions: {
      enabled: false
    }
  },
  navigator: {
    enabled: false
  },
  plotOptions: {
    column: {
      events: {
        legendItemClick() {
          return false;
        }
      }
    },
    series: {
      stacking: 'normal'
    }
  },
  tooltip: {
    enabled: false
  },
  series: null,
  yAxis: {
    allowDecimals: false,
    labels: {
      enabled: false
    },
    gridLineWidth: 0
  },
  xAxis: {
    gridLineWidth: 1,
    showEmpty: false
  },
  lang: {
    noData: 'No data'
  }
});
