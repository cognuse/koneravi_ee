export const exerciseOneSettings = {
  en: [
    {
      mapping: 'recommendedLength',
      label: 'Recommended length of practice',
      options: [
        '1 minute',
        '2 minutes',
        '3 minutes',
        '4 minutes',
        '5 minutes',
        '6 minutes',
        '7 minutes',
        '8 minutes',
        '9 minutes',
        '10 minutes',
        '11 minutes',
        '12 minutes',
        '13 minutes',
        '14 minutes',
        '15 minutes'
      ]
    },
    {
      mapping: 'letters',
      label: 'Letters',
      options: [
        'Capital letters',
        'Uppercase',
        'As is'
      ]
    },
    {
      mapping: 'isRandom',
      label: 'Show in random order',
      options: [
        'Yes',
        'No'
      ]
    },
    {
      mapping: 'correctAnswerFormat',
      label: 'Show correct answer as',
      options: [
        'Word/Sentence',
        'Dotted',
        'None'
      ]
    }
  ],
  et: [
    {
      mapping: 'recommendedLength',
      label: 'Soovitatav harjutamise kestvus',
      options: [
        '1 minut',
        '2 minutit',
        '3 minutit',
        '4 minutit',
        '5 minutit',
        '6 minutit',
        '7 minutit',
        '8 minutit',
        '9 minutit',
        '10 minutit',
        '11 minutit',
        '12 minutit',
        '13 minutit',
        '14 minutit',
        '15 minutit'
      ]
    },
    {
      mapping: 'letters',
      label: 'Tähed',
      options: [
        'Algab suure tähega',
        'Trükitähed',
        'Nii nagu sisestatud'
      ]
    },
    {
      mapping: 'isRandom',
      label: 'Kas valikvastused tuleb ajada segamini?',
      options: [
        'Jah',
        'Ei'
      ]
    },
    {
      mapping: 'correctAnswerFormat',
      label: 'Kuva õige vastus',
      options: [
        'Sõnana või lausena',
        'Punktiirina',
        'Tühi'
      ]
    }
  ]
};

export const exerciseThreeSettings = {
  en: [
    {
      mapping: 'recommendedLength',
      label: 'Recommended length of practice',
      options: [
        '1 minute',
        '2 minutes',
        '3 minutes',
        '4 minutes',
        '5 minutes',
        '6 minutes',
        '7 minutes',
        '8 minutes',
        '9 minutes',
        '10 minutes',
        '11 minutes',
        '12 minutes',
        '13 minutes',
        '14 minutes',
        '15 minutes'
      ]
    },
    {
      mapping: 'isRandom',
      label: 'Show in random order',
      options: [
        'Yes',
        'No'
      ]
    },
    {
      mapping: 'inverted',
      label: 'Audio ',
      options: [
        'Play audio before showing pictures',
        'Play audio at the same time with showing pictures'
      ]
    }
  ],
  et: [
    {
      mapping: 'recommendedLength',
      label: 'Soovitatav harjutamise kestvus',
      options: [
        '1 minut',
        '2 minutit',
        '3 minutit',
        '4 minutit',
        '5 minutit',
        '6 minutit',
        '7 minutit',
        '8 minutit',
        '9 minutit',
        '10 minutit',
        '11 minutit',
        '12 minutit',
        '13 minutit',
        '14 minutit',
        '15 minutit'
      ]
    },
    {
      mapping: 'isRandom',
      label: 'Kas valikvastused tuleb ajada segamini?',
      options: [
        'Jah',
        'Ei'
      ]
    },
    {
      mapping: 'inverted',
      label: 'Audio',
      options: [
        'Mängi audio enne piltide näitamist',
        'Mängi audio piltidega samaaegselt'
      ]
    }
  ]
};


export const exerciseFiveSettings = {
  en: [
    {
      mapping: 'recommendedLength',
      label: 'Recommended length of practice',
      options: [
        '1 minute',
        '2 minutes',
        '3 minutes',
        '4 minutes',
        '5 minutes',
        '6 minutes',
        '7 minutes',
        '8 minutes',
        '9 minutes',
        '10 minutes',
        '11 minutes',
        '12 minutes',
        '13 minutes',
        '14 minutes',
        '15 minutes'
      ]
    },
    {
      mapping: 'delay',
      label: 'Next exercise loading speed',
      options: [
        'Slow',
        'Normal',
        'Fast'
      ]
    },
    {
      mapping: 'hideAnswer',
      label: 'Hide correct answer',
      options: [
        'Yes',
        'No'
      ]
    },
    {
      mapping: 'letters',
      label: 'Letters',
      options: [
        'Capital letters',
        'Uppercase',
        'As is'
      ]
    },
    {
      mapping: 'numberOfAnswers',
      label: 'Default number of answers',
      options: [1, 2, 3, 4, 5, 6, 7, 8]
    }
  ],
  et: [
    {
      mapping: 'recommendedLength',
      label: 'Soovitatav harjutamise kestvus',
      options: [
        '1 minut',
        '2 minutit',
        '3 minutit',
        '4 minutit',
        '5 minutit',
        '6 minutit',
        '7 minutit',
        '8 minutit',
        '9 minutit',
        '10 minutit',
        '11 minutit',
        '12 minutit',
        '13 minutit',
        '14 minutit',
        '15 minutit'
      ]
    },
    {
      mapping: 'delay',
      label: 'Järgmise harjutuse laadimise kiirus',
      options: [
        'Aeglane',
        'Keskmine',
        'Kiire'
      ]
    },
    {
      mapping: 'hideAnswer',
      label: 'Kas peita õige vastus peale sellele vajutamist?',
      options: [
        'Jah',
        'Ei'
      ]
    },
    {
      mapping: 'letters',
      label: 'Tähed',
      options: [
        'Algab suure tähega',
        'Trükitähed',
        'Nii nagu sisestatud'
      ]
    },
    {
      mapping: 'numberOfAnswers',
      label: 'Mitu vastust korraga kuvada?',
      options: [1, 2, 3, 4, 5, 6, 7, 8]
    }
  ]
};

export const exerciseFiveASettings = {
  en: [
    {
      mapping: 'recommendedLength',
      label: 'Recommended length of practice',
      options: [
        '1 minute',
        '2 minutes',
        '3 minutes',
        '4 minutes',
        '5 minutes',
        '6 minutes',
        '7 minutes',
        '8 minutes',
        '9 minutes',
        '10 minutes',
        '11 minutes',
        '12 minutes',
        '13 minutes',
        '14 minutes',
        '15 minutes'
      ]
    },
    {
      mapping: 'delay',
      label: 'Next exercise loading speed',
      options: [
        'Slow',
        'Normal',
        'Fast'
      ]
    },
    {
      mapping: 'hideAnswer',
      label: 'Hide correct answer',
      options: [
        'Yes',
        'No'
      ]
    },
    {
      mapping: 'letters',
      label: 'Letters',
      options: [
        'Capital letters',
        'Uppercase',
        'As is'
      ]
    },
    {
      mapping: 'inverted',
      label: 'Order of optional answers and sentence',
      options: [
        'Sentence before the optional answers',
        'Optional answers before the sentence'
      ]
    },
    {
      mapping: 'numberOfAnswers',
      label: 'Default number of answers',
      options: [1, 2, 3, 4, 5, 6, 7, 8]
    }
  ],
  et: [
    {
      mapping: 'recommendedLength',
      label: 'Soovitatav harjutamise kestvus',
      options: [
        '1 minut',
        '2 minutit',
        '3 minutit',
        '4 minutit',
        '5 minutit',
        '6 minutit',
        '7 minutit',
        '8 minutit',
        '9 minutit',
        '10 minutit',
        '11 minutit',
        '12 minutit',
        '13 minutit',
        '14 minutit',
        '15 minutit'
      ]
    },
    {
      mapping: 'delay',
      label: 'Järgmise harjutuse laadimise kiirus',
      options: [
        'Aeglane',
        'Keskmine',
        'Kiire'
      ]
    },
    {
      mapping: 'hideAnswer',
      label: 'Kas peita õige vastus peale sellele vajutamist?',
      options: [
        'Jah',
        'Ei'
      ]
    },
    {
      mapping: 'letters',
      label: 'Tähed',
      options: [
        'Algab suure tähega',
        'Trükitähed',
        'Nii nagu sisestatud'
      ]
    },
    {
      mapping: 'inverted',
      label: 'Valikvastuste ja lause järjekord',
      options: [
        'Lause enne valikvastuseid',
        'Valikvastused enne lauset'
      ]
    },
    {
      mapping: 'numberOfAnswers',
      label: 'Mitu vastust korraga kuvada?',
      options: [1, 2, 3, 4, 5, 6, 7, 8]
    }
  ]
};


export const exerciseSixSettings = {
  en: [
    {
      mapping: 'recommendedLength',
      label: 'Recommended length of practice',
      options: [
        '1 minute',
        '2 minutes',
        '3 minutes',
        '4 minutes',
        '5 minutes',
        '6 minutes',
        '7 minutes',
        '8 minutes',
        '9 minutes',
        '10 minutes',
        '11 minutes',
        '12 minutes',
        '13 minutes',
        '14 minutes',
        '15 minutes'
      ]
    },
    {
      mapping: 'delay',
      label: 'Next exercise loading speed',
      options: [
        'Slow',
        'Normal',
        'Fast'
      ]
    },
    {
      mapping: 'letters',
      label: 'Letters',
      options: [
        'Capital letters',
        'Uppercase',
        'As is'
      ]
    },
    {
      mapping: 'isValidated',
      label: 'Automatic validation',
      options: [
        'Yes',
        'No'
      ]
    }
  ],
  et: [
    {
      mapping: 'recommendedLength',
      label: 'Soovitatav harjutamise kestvus',
      options: [
        '1 minut',
        '2 minutit',
        '3 minutit',
        '4 minutit',
        '5 minutit',
        '6 minutit',
        '7 minutit',
        '8 minutit',
        '9 minutit',
        '10 minutit',
        '11 minutit',
        '12 minutit',
        '13 minutit',
        '14 minutit',
        '15 minutit'
      ]
    },
    {
      mapping: 'delay',
      label: 'Järgmise harjutuse laadimise kiirus',
      options: [
        'Aeglane',
        'Keskmine',
        'Kiire'
      ]
    },
    {
      mapping: 'letters',
      label: 'Tähed',
      options: [
        'Algab suure tähega',
        'Trükitähed',
        'Nii nagu sisestatud'
      ]
    },
    {
      mapping: 'isValidated',
      label: 'Kas vastuseid tuleb kontrollida automaatselt?',
      options: [
        'Jah',
        'Ei'
      ]
    }
  ]
};

export const exerciseSevenSettings = {
  en: [
    {
      mapping: 'recommendedLength',
      label: 'Recommended length of practice',
      options: [
        '1 minute',
        '2 minutes',
        '3 minutes',
        '4 minutes',
        '5 minutes',
        '6 minutes',
        '7 minutes',
        '8 minutes',
        '9 minutes',
        '10 minutes',
        '11 minutes',
        '12 minutes',
        '13 minutes',
        '14 minutes',
        '15 minutes'
      ]
    },
    {
      mapping: 'isRandom',
      label: 'Show in random order',
      options: [
        'Yes',
        'No'
      ]
    },
    {
      mapping: 'isValidated',
      label: 'Automatic validation',
      options: [
        'Yes',
        'No'
      ]
    },
    {
      mapping: 'delay',
      label: 'Next exercise loading speed',
      options: [
        'Slow',
        'Normal',
        'Fast'
      ]
    },
    {
      mapping: 'letters',
      label: 'Letters',
      options: [
        'Capital letters',
        'Uppercase',
        'As is'
      ]
    }
  ],
  et: [
    {
      mapping: 'recommendedLength',
      label: 'Soovitatav harjutamise kestvus',
      options: [
        '1 minut',
        '2 minutit',
        '3 minutit',
        '4 minutit',
        '5 minutit',
        '6 minutit',
        '7 minutit',
        '8 minutit',
        '9 minutit',
        '10 minutit',
        '11 minutit',
        '12 minutit',
        '13 minutit',
        '14 minutit',
        '15 minutit'
      ]
    },
    {
      mapping: 'isRandom',
      label: 'Kas valikvastused tuleb ajada segamini?',
      options: [
        'Jah',
        'Ei'
      ]
    },
    {
      mapping: 'isValidated',
      label: 'Kas vastuseid tuleb kontrollida automaatselt?',
      options: [
        'Jah',
        'Ei'
      ]
    },
    {
      mapping: 'delay',
      label: 'Järgmise harjutuse laadimise kiirus',
      options: [
        'Aeglane',
        'Keskmine',
        'Kiire'
      ]
    },
    {
      mapping: 'letters',
      label: 'Tähed',
      options: [
        'Algab suure tähega',
        'Trükitähed',
        'Nii nagu sisestatud'
      ]
    }
  ]
};

export const exerciseEightSettings = {
  en: [
    {
      mapping: 'recommendedLength',
      label: 'Recommended length of practice',
      options: [
        '1 minute',
        '2 minutes',
        '3 minutes',
        '4 minutes',
        '5 minutes',
        '6 minutes',
        '7 minutes',
        '8 minutes',
        '9 minutes',
        '10 minutes',
        '11 minutes',
        '12 minutes',
        '13 minutes',
        '14 minutes',
        '15 minutes'
      ]
    },
    {
      mapping: 'audioSpeed',
      label: 'Speed of the audio',
      options: [
        'Slow',
        'Normal',
        'Fast'
      ]
    }
  ],
  et: [
    {
      mapping: 'recommendedLength',
      label: 'Soovitatav harjutamise kestvus',
      options: [
        '1 minut',
        '2 minutit',
        '3 minutit',
        '4 minutit',
        '5 minutit',
        '6 minutit',
        '7 minutit',
        '8 minutit',
        '9 minutit',
        '10 minutit',
        '11 minutit',
        '12 minutit',
        '13 minutit',
        '14 minutit',
        '15 minutit'
      ]
    },
    {
      mapping: 'audioSpeed',
      label: 'Heli mängimise kiirus',
      options: [
        'Aeglane',
        'Keskmine',
        'Kiire'
      ]
    }
  ]
};

export const exerciseNineSettings = {
  en: [
    {
      mapping: 'recommendedLength',
      label: 'Recommended length of practice',
      options: [
        '1 minute',
        '2 minutes',
        '3 minutes',
        '4 minutes',
        '5 minutes',
        '6 minutes',
        '7 minutes',
        '8 minutes',
        '9 minutes',
        '10 minutes',
        '11 minutes',
        '12 minutes',
        '13 minutes',
        '14 minutes',
        '15 minutes'
      ]
    },
    {
      mapping: 'delay',
      label: 'Next exercise loading speed',
      options: [
        'Slow',
        'Normal',
        'Fast'
      ]
    },
    {
      mapping: 'numberOfAnswers',
      label: 'Number of maximum pairs',
      options: [1, 2, 3, 4, 5, 6, 7, 8]
    },
    {
      mapping: 'letters',
      label: 'Letters',
      options: [
        'Capital letters',
        'Uppercase',
        'As is'
      ]
    },
    {
      mapping: 'isValidated',
      label: 'Automatic validation',
      options: [
        'Yes',
        'No'
      ]
    }
  ],
  et: [
    {
      mapping: 'recommendedLength',
      label: 'Soovitatav harjutamise kestvus',
      options: [
        '1 minut',
        '2 minutit',
        '3 minutit',
        '4 minutit',
        '5 minutit',
        '6 minutit',
        '7 minutit',
        '8 minutit',
        '9 minutit',
        '10 minutit',
        '11 minutit',
        '12 minutit',
        '13 minutit',
        '14 minutit',
        '15 minutit'
      ]
    },
    {
      mapping: 'delay',
      label: 'Järgmise harjutuse laadimise kiirus',
      options: [
        'Aeglane',
        'Keskmine',
        'Kiire'
      ]
    },
    {
      mapping: 'numberOfAnswers',
      label: 'Mitu vastust korraga kuvada?',
      options: [1, 2, 3, 4, 5, 6, 7, 8]
    },
    {
      mapping: 'letters',
      label: 'Tähed',
      options: [
        'Algab suure tähega',
        'Trükitähed',
        'Nii nagu sisestatud'
      ]
    },
    {
      mapping: 'isValidated',
      label: 'Kas vastuseid tuleb kontrollida automaatselt?',
      options: [
        'Jah',
        'Ei'
      ]
    }
  ]
};


// type, defaultValue, id, options[].value should not be changed
// title, options[].label are for localisation
export const filterSettings = {
  en: [
    {
      type: 'radio',
      title: 'Exercises',
      defaultValue: 'all exercises',
      id: 'exercise',
      options:
        [
          {
            label: 'All exercises',
            value: 'all exercises',
            name: 'exercise'
          },
          {
            label: 'Favourites',
            value: 'favourites',
            name: 'exercise'
          }
        ]
    },
    {
      type: 'radio',
      title: 'Access',
      defaultValue: 'mine',
      id: 'access',
      options: [
        {
          label: 'Created by me',
          value: 'mine',
          name: 'access'
        },
        {
          label: 'Created by others',
          value: 'others',
          name: 'access'
        }
      ]
    },
    {
      type: 'checkbox',
      title: 'Exercise type',
      defaultValue: '',
      id: 'type',
      options: [
        {
          label: 'Automatism',
          value: 'automatism'
        },
        {
          label: 'Word',
          value: 'word'
        },
        {
          label: 'Phrases',
          value: 'phrases'
        },
        {
          label: 'Sentences',
          value: 'sentences'
        },
        {
          label: 'Texts',
          value: 'texts'
        }
      ]
    },
    {
      type: 'checkbox',
      title: 'Based on syndromes',
      defaultValue: '',
      id: 'languageDomain',
      options: [
        {
          label: 'Pronunciation',
          value: 'pronunciation'
        },
        {
          label: 'Vocabulary',
          value: 'vocabulary'
        },
        {
          label: 'Grammar',
          value: 'grammar'
        },
        {
          label: 'Text',
          value: 'text'
        },
        {
          label: 'Communication',
          value: 'communication'
        },
        {
          label: 'Reading',
          value: 'reading'
        },
        {
          label: 'Writing',
          value: 'writing'
        }
      ]
    }
  ],
  et: [
    {
      type: 'radio',
      title: 'Harjutused',
      defaultValue: 'all exercises',
      id: 'exercise',
      options:
        [
          {
            label: 'Kõik harjutused',
            value: 'all exercises',
            name: 'exercise'
          },
          {
            label: 'Ainult lemmikud',
            value: 'favourites',
            name: 'exercise'
          }
        ]
    },
    {
      type: 'radio',
      title: 'Harjutuse jagamine',
      defaultValue: 'mine',
      id: 'access',
      options: [
        {
          label: 'Minu loodud',
          value: 'mine',
          name: 'access'
        },
        {
          label: 'Teiste loodud',
          value: 'others',
          name: 'access'
        }
      ]
    },
    {
      type: 'checkbox',
      title: 'Harjutuste tüübid',
      defaultValue: '',
      id: 'type',
      options: [
        {
          label: 'Kõneautomatism',
          value: 'kõneautomatism'
        },
        {
          label: 'Sõna',
          value: 'sõna'
        },
        {
          label: 'Sõnaühendid',
          value: 'sõnaühendid'
        },
        {
          label: 'Lause',
          value: 'laused'
        },
        {
          label: 'Tekst',
          value: 'tekst'
        }
      ]
    },
    {
      type: 'checkbox',
      title: 'Keeletasandid',
      defaultValue: '',
      id: 'languageDomain',
      options: [
        {
          label: 'Hääldamine',
          value: 'hääldamine'
        },
        {
          label: 'Sõnavara',
          value: 'sõnavara'
        },
        {
          label: 'Grammatika',
          value: 'grammatika'
        },
        {
          label: 'Tekst',
          value: 'tekst'
        },
        {
          label: 'Suhtlemine',
          value: 'suhtlemine'
        },
        {
          label: 'Lugemine',
          value: 'lugemine'
        },
        {
          label: 'Kirjutamine',
          value: 'kirjutamine'
        }
      ]
    }
  ]
};

// id, visible should not be changed
// title, supportingText are for localisation
export const exerciseContentSections = {
  en: [
    {
      title: 'Automatism exercises',
      supportingText: 'Lorem ipsum dolor sit amet, his verear mentitum in, nec commune molestiae disputando te. Eum cu agam justo, error quaerendum pro an, vel vocent option numquam ei. Nec utinam senserit ad, ea referrentur dissentiunt mel. Qui nulla homero eleifend cu.',
      id: 'automatism',
      visible: false
    },
    {
      title: 'Word exercises',
      supportingText: 'Lorem ipsum dolor sit amet, his verear mentitum in, nec commune molestiae disputando te. Eum cu agam justo, error quaerendum pro an, vel vocent option numquam ei. Nec utinam senserit ad, ea referrentur dissentiunt mel. Qui nulla homero eleifend cu.',
      id: 'word',
      visible: false
    },
    {
      title: 'Exercises with phrases',
      supportingText: 'Lorem ipsum dolor sit amet, his verear mentitum in, nec commune molestiae disputando te. Eum cu agam justo, error quaerendum pro an, vel vocent option numquam ei. Nec utinam senserit ad, ea referrentur dissentiunt mel. Qui nulla homero eleifend cu.',
      id: 'phrases',
      visible: false
    },
    {
      title: 'Exercises with sentences',
      supportingText: 'Lorem ipsum dolor sit amet, his verear mentitum in, nec commune molestiae disputando te. Eum cu agam justo, error quaerendum pro an, vel vocent option numquam ei. Nec utinam senserit ad, ea referrentur dissentiunt mel. Qui nulla homero eleifend cu.',
      id: 'sentences',
      visible: false
    },
    {
      title: 'Text based exercises',
      supportingText: 'Lorem ipsum dolor sit amet, his verear mentitum in, nec commune molestiae disputando te. Eum cu agam justo, error quaerendum pro an, vel vocent option numquam ei. Nec utinam senserit ad, ea referrentur dissentiunt mel. Qui nulla homero eleifend cu.',
      id: 'texts',
      visible: false
    }
  ],
  et: [
    {
      title: 'Kõneautomatismi harjutused',
      supportingText: 'Kui inimesel on tahtlik kõne puudu,  alustatakse tööd tahtmatu ehk automatiseerunud kõne taastamisest. Selle abil kõrvaldada kaitsepidurdust.  Harjutuste hulgast leiab   numbrite, nädalapäevade, kuunimetuste, värvuste,  tuntud kõnekäändude, luuletuste, laulude nimetamise.',
      id: 'kõneautomatism',
      visible: false
    },
    {
      title: 'Sõna harjutused',
      supportingText: 'Sõna on  keeleüksus võimaldades moodustada lauseid ja väljendada mõtteid. Sõna on samuti osa tunnetustegevusest tähistades kujutlust või mõistet. Sõnavara omandamisel on oluline nii sõnavara suurus kui koostis (sõnaliigid ja nende proportsioonid), sõnade tähenduse mõistmine, sõnade muutevormide kasutamine. Sõnavara annab tuummaterjali kõne arendamiseks  ja/või taastamiseks. Sõnade omandamiseks ja taastamiseks on keskkonnas olev sõnavara jaotatud teemakaustadesse. Sõna tasandi harjutusi saabki teha ka teemade kaupa. Harjutused treenivad nii sõnade nimetamisoskust kui sõna tähenduse mõistmist.',
      id: 'sõna',
      visible: false
    },
    {
      title: 'Sõnaühenditega harjutused',
      supportingText: 'Sõnaühend on grammatiliselt seostuvate iseseisvate sõnade ühendus. Harjutustikus on harjutusi sõnaühendite moodustamiseks nt tegusõna ja nimisõna (mõõdab pikkust).',
      id: 'sõnaühendid',
      visible: false
    },
    {
      title: 'Lause harjutused',
      supportingText: 'Teema sisaldab nii sõnade vormistikku kui lauset käsitlevaid harjutusi. Lause kaudu omandab inimene sõnavara ja sõnade vormistiku.  Lauselist kõnet arendavad harjutused on nt sõnade panemine lausesse õigesse järjekorda, liigse lauseliikme leidmine, lause kirjalik vormistamine.',
      id: 'laused',
      visible: false
    },
    {
      title: 'Tekstiharjutused',
      supportingText: 'Tekst on ühe teema kohta rohkem kui ühe lause loomine. Teksti loome algab soovist või vajadusest kellelegi mingil eesmärgil teatada. Teksti mõistmise aluseks on sõnatähenduse tundmine, lausete mõistmine, lausete tähenduste ja mõtete ühendamine. Harjutustikust leiab nt lausete seadmise loogilisse järjekorda, tuntud laulude ja luuletuste kokku panemise. ',
      id: 'tekst',
      visible: false
    }
  ]
};
