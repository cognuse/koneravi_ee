import axios from 'axios';
//TODO: use thunk third argument applyMiddleware(thunk.withExtraArgument(arg)
//utils, data
import { sortBy } from 'lodash';
import { getContentQuery } from '../../common/queries';

// api
import {
  GRAPHQL_URL,
  GRAPHQL_URL_RESULTS,
  AUDIOS_URL,
  RESULTS_URL,
  REPORT_RESULTS_URL,
  apiConfig
} from '../../common/api';

//constants
import {
  ADD_TO_FAVOURITES,
  SET_SECTIONS,
  SET_FILTERS,
  SET_TOPICS,
  MODIFY_EXERCISES,
  MODIFY_SECTIONS,
  MODIFY_CONTENT,
  MODIFY_ACCESS,
  RESET_EXERCISES,
  GET_EXERCISE_PREVIEW,
  GET_EXERCISES,
  GET_SINGLE_EXERCISE,
  GET_TEMPLATE_ONE_EXERCISE,
  GET_TEMPLATE_THREE_EXERCISE,
  GET_TEMPLATE_SIX_EXERCISE,
  GET_TEMPLATE_NINE_EXERCISE,
  UPDATE_SETTINGS,
  UPDATE_TESTS,
  RESET_CURRENT_EXERCISE,
  RESET_LAST_EXERCISE,
  SHOW_OVERLAY,
  TEMPLATES_GET_SKIPPED_WORDS_OPTIONS,
  SET_LANGUAGE_FILTER,
  SET_SELECTED_TOPIC,
  EXERCISE_DURATION,
  EXERCISE_RESULT
} from './constants';

export function addToFavourites(exercise) {
  //send exercise._id to the server

  return {
    type: ADD_TO_FAVOURITES,
    payload: exercise
  };
}

export function setSelectedTopic(topic) {
  return {
    type: SET_SELECTED_TOPIC,
    payload: topic
  };
}

export function setFilters(filterObj) {
  return {
    type: SET_FILTERS,
    payload: filterObj
  };
}

export function setSections(sections) {
  return {
    type: SET_SECTIONS,
    payload: sections
  };
}

export function modifyExercises(modifier, value) {
  return (dispatch) => {
    dispatch({
      type: SET_LANGUAGE_FILTER,
      payload: {
        modifier,
        value
      }
    });
    dispatch({
      type: MODIFY_EXERCISES
    });
  };
}

export function modifySections(modifier) {
  return {
    type: MODIFY_SECTIONS,
    payload: modifier
  };
}

const __modifyAccess = (modifier, id) => ({
  type: MODIFY_ACCESS,
  payload: {
    modifier,
    id
  }
});

export function modifyAccess(modifier) {
  return (dispatch, getState) => {
    dispatch(__modifyAccess(modifier, getState().User.currentUser.id));
  };
}

export function modifyContent(modifier) {
  return (dispatch, getState) => {
    dispatch({
      type: MODIFY_CONTENT,
      payload: modifier
    });
    dispatch(__modifyAccess(getState().Exercises.accessType, getState().User.currentUser.id));
  };
}

export function getExercises(queryObj) {
  return (dispatch, getState) => {
    const { id, token, role } = getState().User.currentUser;
    axios.post(GRAPHQL_URL, queryObj, apiConfig(token))
      .then(result => {
        const exercises = result.data.data.exercises;
        const modifier = exercises.some(el => el.createdBy === id) ?
          'mine' : 'others';

        dispatch({
          type: GET_EXERCISES,
          payload: {
            id,
            role,
            exercises
          }
        });

        dispatch(__modifyAccess(modifier, id));
      }, error => {
        throw new Error(`rejected ${error.message}`);
      });
  };
}

export const updateDuration = (duration) => ({
  type: EXERCISE_DURATION,
  payload: duration
});

export const updateResult = (correct) => ({
  type: EXERCISE_RESULT,
  payload: correct
});

export function saveResults(results) {
  return (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    dispatch(updateDuration(results.variables.input.timeTotal));
    dispatch(updateResult(+results.variables.input.correct));

    axios.post(GRAPHQL_URL_RESULTS, results, apiConfig(token))
      .then(() => {
      }, error => {
        throw new Error(`rejected ${error.message}`);
      });
  };
}

export function getExercisePreview(queryObj, groupsQuery) {
  return (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    Promise.all([
      axios.post(GRAPHQL_URL, queryObj, apiConfig(token)),
      axios.post(GRAPHQL_URL, groupsQuery, apiConfig(token))
    ]).then(result => {
      dispatch({
        type: GET_EXERCISE_PREVIEW,
        payload: result[0].data.data.exercise
      });

      if (!result[1].data.data.exerciseGroups || result[1].data.data.exerciseGroups.some(el => !el.title)) {
        return;
      }
      dispatch({
        type: SET_TOPICS,
        payload: result[1].data.data.exerciseGroups
      });
    }, error => {
      throw new Error(`rejected ${error.message}`);
    });
  };
}

export function getExercise(exerciseQuery, viewsQuery) {
  return (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    const config = apiConfig(token);
    return Promise.all([
      axios.post(GRAPHQL_URL, exerciseQuery, config),
      axios.post(GRAPHQL_URL, viewsQuery, config)
    ])
      .then(result => {
        const exerciseData = result[0].data.data.exercise;
        const views = result[1].data.data.views;
        const exercise = Object.assign({}, exerciseData, { views });

        dispatch({
          type: getActionType(exercise.template),
          payload: exercise
        });

        return exercise;
      }, error => {
        throw new Error(`rejected ${error.message}`);
      });
  };
}

function getActionType(template) {
  switch (template) {
    case '1':
      return GET_TEMPLATE_ONE_EXERCISE;
    case '3':
      return GET_TEMPLATE_THREE_EXERCISE;
    case '6':
      return GET_TEMPLATE_SIX_EXERCISE;
    case '9':
      return GET_TEMPLATE_NINE_EXERCISE;
    default:
      return GET_SINGLE_EXERCISE;
  }
}

export function getOptions(locale) {
  return (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    const queryList = getContentQuery(locale);

    return Promise.all(
      queryList.map(query => axios.post(GRAPHQL_URL, query, apiConfig(token)))
    )
      .then(result => {
        const payload = {};
        result.forEach(({ data: { data } }) => (
          Object.assign(payload, {
            [data.content[0].topic]: sortBy(data.content, 'order')
              .map(({ order, topic, ...rest }) => rest)
          })
        ));
        dispatch({
          type: TEMPLATES_GET_SKIPPED_WORDS_OPTIONS,
          payload
        });
      });
  };
}

export function getExerciseStatistics(query) {
  return (dispatch, getState) => {
    if (!query.variables.createdBy) {
      console.warn('patient or user id is missing');
      return;
    }
    if (!query.variables.exercise) {
      console.warn('exercise id is missing');
      return;
    }
    const locale = getState().User.currentUser.locale;
    const token = getState().User.currentUser.token;
    return axios.post(GRAPHQL_URL_RESULTS, query, apiConfig(token))
      .then(({ data: { data: { aggregateResultsByDate } } }) => {
        if (!aggregateResultsByDate.length) return null;
        const correctSum = aggregateResultsByDate.reduce((a, c) => a + c.correctCount, 0);
        const incorrectSum = aggregateResultsByDate.reduce((a, c) => a + (c.count - c.correctCount), 0);
        return (!correctSum || !incorrectSum)
          ? ([
              {
                type: 'column',
                name: locale === 'et' ? 'läbitud harjutusi' : 'exercises completed',
                color: '#FF87B0',
                className: 'columnChart',
                data: sortBy(aggregateResultsByDate.map(({ created, timeTotal }) => ({
                    dataLabels: {
                      enabled: true,
                      inside: false,
                      style: {
                        fontWeight: 'bold',
                        fontSize: '14px',
                        color: 'black'
                      },
                      formatter() {
                        return `${this.total} min`;
                      },
                      useHTML: true
                    },
                    x: Date.parse(created),
                    y: Math.ceil(timeTotal / 60)
                  })
                ), 'x')
              }
            ])
          : ([
              {
                type: 'column',
                name: ' ',
                color: 'transparent',
                dataLabels: {
                  enabled: true,
                  inside: false,
                  style: {
                    fontWeight: 'bold',
                    fontSize: '14px',
                    color: 'black'
                  },
                  formatter() {
                    return `${this.total} min`;
                  },
                  useHTML: false
                },
                data: sortBy(aggregateResultsByDate.map(({ correctCount, count, created }) => ({
                    x: Date.parse(created),
                    y: 0,
                    correct: correctCount,
                    count
                  })
                ), 'x')
              },
              {
                type: 'column',
                name: locale === 'et' ? 'valesid vastuseid' : 'wrong answers',
                color: '#65CAEF',
                className: 'columnChart',
                data: sortBy(aggregateResultsByDate.map(({ correctCount, count, created, timeTotal }) => ({
                    dataLabels: {
                      enabled: true,
                      inside: true,
                      visible: false,
                      style: {
                        fontWeight: 'bold',
                        fontSize: '14px',
                        stroke: 'none',
                        color: (count - correctCount) ? 'white' : 'transparent',
                      },
                      formatter() {
                        return `<span>${count - correctCount}</span>`;
                      },
                      useHTML: true
                    },
                    x: Date.parse(created),
                    y: Math.ceil(timeTotal / 60) * (1 - (correctCount / count)).toPrecision(2)
                  })
                ), 'x')
              },
              {
                type: 'column',
                name: locale === 'et' ? 'õigeid vastuseid' : 'correct answers',
                color: '#21B14D',
                className: 'columnChart',
                data: sortBy(aggregateResultsByDate.map(({ correctCount, count, created, timeTotal }) => ({
                    dataLabels: {
                      enabled: true,
                      inside: true,
                      style: {
                        fontWeight: 'bold',
                        fontSize: '14px',
                        stroke: 'none',
                        color: correctCount ? 'white' : 'transparent',
                      },
                      formatter() {
                        return `<span>${this.point.correct}</span>`;
                      },
                      useHTML: true
                    },
                    x: Date.parse(created),
                    y: Math.ceil(timeTotal / 60) * (correctCount / count).toPrecision(2),
                    correct: correctCount,
                    count
                  })
                ), 'x')
              }
            ]);
      })
      .catch(err => {
        console.error(err);
      });
  };
}
export function getTotalExerciseTime(query) {
  return (dispatch, getState) => {
    if (!query.variables.createdBy) {
      console.warn('patient or user id is missing');
      return;
    }
    if (!query.variables.exercise) {
      console.warn('exercise id is missing');
      return;
    }
    const token = getState().User.currentUser.token;
    return axios.post(GRAPHQL_URL_RESULTS, query, apiConfig(token))
      .then(result => result.data.data.countResults.timeTotal)
      .catch(err => {
        console.error(err);
      });
  };
}

export function sendReport(queryObj) {
  return (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    axios.post(GRAPHQL_URL, queryObj, apiConfig(token))
      .then(result => {
        console.log('result data', result.data);
      })
      .catch(err => {
        console.error(err);
      });
  };
}

export function sendResults(queryObj) {
  return (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    axios.post(GRAPHQL_URL, queryObj, apiConfig(token))
      .then(result => {
        console.log('result data', result.data);
      })
      .catch(err => {
        console.error(err);
      });
  };
}

export function resetCurrentExercise() {
  return {
    type: RESET_CURRENT_EXERCISE
  };
}

export function resetLastExerciseData() {
  return {
    type: RESET_LAST_EXERCISE
  };
}

export function resetExercises() {
  return {
    type: RESET_EXERCISES
  };
}

export function showOverlay(status = true) {
  return {
    type: SHOW_OVERLAY,
    payload: status
  };
}

export function updateCurrentSettings(payload) {
  return {
    payload,
    type: UPDATE_SETTINGS
  };
}

export function updateTests(payload) {
  return {
    payload,
    type: UPDATE_TESTS
  };
}

export function uploadBlob(blob, fileName = 'testName') {
  if (!blob) return;
  axios.post(AUDIOS_URL, {
    filename: fileName,
    filetype: blob.type
  }).then(result => {
    if (result.request.readyState === 4 && result.status === 200) {
      upload(blob, result.data.signed);
    }
  });

  return {
    type: ''
  };
}

function upload(blob, signRequest) {
  const xhr = new XMLHttpRequest();
  xhr.open('PUT', signRequest);
  xhr.setRequestHeader('x-amz-acl', 'public-read');
  xhr.onload = function () {
    if (xhr.status === 200) {
      console.log('file successfully uploaded');
    }
  };
  xhr.send(blob);
}
