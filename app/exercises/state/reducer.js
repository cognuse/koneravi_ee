//utils
import { handleActions } from 'redux-actions';
import shortid from 'shortid';
import {
  shuffleArray,
  replaceWithBigger,
  getAudioUrl,
  filterByLanguage,
  setFiltersAmount
} from '../helpers';

//constants
import {
  ADD_TO_FAVOURITES,
  SET_SECTIONS,
  SET_FILTERS,
  SET_TOPICS,
  MODIFY_EXERCISES,
  MODIFY_SECTIONS,
  MODIFY_CONTENT,
  MODIFY_ACCESS,
  RESET_EXERCISES,
  RESET_LAST_EXERCISE,
  GET_EXERCISE_PREVIEW,
  GET_EXERCISES,
  GET_SINGLE_EXERCISE,
  GET_TEMPLATE_ONE_EXERCISE,
  GET_TEMPLATE_THREE_EXERCISE,
  GET_TEMPLATE_SIX_EXERCISE,
  GET_TEMPLATE_NINE_EXERCISE,
  UPDATE_SETTINGS,
  UPDATE_TESTS,
  RESET_CURRENT_EXERCISE,
  SHOW_OVERLAY,
  TEMPLATES_GET_SKIPPED_WORDS_OPTIONS,
  SET_LANGUAGE_FILTER,
  SET_SELECTED_TOPIC,
  EXERCISE_DURATION,
  EXERCISE_RESULT
} from './constants';

export const INITIAL_STATE = {
  sections: [],
  exercises: [],
  currentExercises: [],
  languageFilter: {},
  favourites: [],
  currentFavourites: [],
  currentExercise: {},
  currentSettings: [],
  tests: [],
  backupTests: [],
  overlay: false,
  contentType: 'all exercises',
  accessType: '',
  filters: {},
  selectedTopic: '',
  topics: [],
  lastExerciseId: '',
  lastExerciseDuration: 0,
  lastExerciseResult: {
    total: 0,
    correct: 0
  }
};

export default handleActions({
  [SET_SELECTED_TOPIC]: (state, action) => (
    {
      ...state,
      selectedTopic: action.payload
    }
  ),
  [SET_LANGUAGE_FILTER]: (state, action) => (
    {
      ...state,
      languageFilter: Object.assign({}, state.languageFilter, { [action.payload.modifier]: action.payload.value }),
    }
  ),
  [ADD_TO_FAVOURITES]: (state, action) => (
    {
      ...state,
      favourites: state.favourites.some((exercise) => exercise._id === action.payload._id) ?
        state.favourites : [...state.favourites, action.payload],
      currentFavourites: state.currentFavourites.some((exercise) => exercise._id === action.payload._id) ?
        state.currentFavourites : [...state.currentFavourites, action.payload]
    }
  ),
  [MODIFY_EXERCISES]: (state) => (
    {
      ...state,
      currentExercises: filterByLanguage(state.exercises, state.languageFilter),
      currentFavourites: filterByLanguage(state.favourites, state.languageFilter)
    }
  ),
  [MODIFY_SECTIONS]: (state, action) => (
    {
      ...state,
      sections: state.sections.map((section) => (
        section.id === action.payload ? Object.assign({}, section, { visible: !section.visible }) : section
      ))
    }
  ),
  [MODIFY_CONTENT]: (state, action) => (
    {
      ...state,
      contentType: action.payload
    }
  ),
  [MODIFY_ACCESS]: (state, action) => {
    let currentExercises = action.payload.modifier === 'mine' ?
      state.exercises.filter(el => el.createdBy === action.payload.id)
      :
      state.exercises.filter(el => el.createdBy !== action.payload.id);
    let currentFavourites = action.payload.modifier === 'mine' ?
      state.favourites.filter(el => el.createdBy === action.payload.id)
      :
      state.favourites.filter(el => el.createdBy !== action.payload.id);
    if (!currentExercises.length) {
      currentExercises = ['noExercises'];
    }
    if (!currentFavourites.length) {
      currentFavourites = ['noExercises'];
    }
    return {
      ...state,
      currentExercises,
      currentFavourites,
      accessType: action.payload.modifier
    };
  },
  [SET_FILTERS]: (state, action) => (
    {
      ...state,
      filters: action.payload
    }
  ),
  [SET_TOPICS]: (state, action) => (
    {
      ...state,
      topics: action.payload.map(topic => topic.title).sort()
    }
  ),
  [SET_SECTIONS]: (state, action) => (
    {
      ...state,
      sections: action.payload
    }
  ),
  [GET_EXERCISES]: (state, action) => {
    const exercises = !action.payload.exercises.length ? ['error'] : action.payload.exercises;
    let currentExercises = action.payload.role === 1 ?
      exercises : exercises.filter(el => el.createdBy === action.payload.id);
    if (!currentExercises.length) {
      currentExercises = ['noExercises'];
    }

    return ({
      ...state,
      currentExercises,
      exercises: action.payload.exercises,
      filters: setFiltersAmount(state.filters, action.payload.exercises)
    });
  },
  [GET_TEMPLATE_SIX_EXERCISE]: (state, action) => (
    {
      ...state,
      currentExercise: Object.assign({}, action.payload),
      tests: Object.assign([], action.payload.views.map((object) =>
        Object.assign(
          {}, object,
          { answers: shuffleArray(object.originalText.split(' ')) },
          { references: object.originalText.split(' ') },
          { image: object.image.replace('small/', '').replace('_small', '') }
        )
      ))
    }
  ),
  [GET_EXERCISE_PREVIEW]: (state, action) => ({ ...state, currentExercise: Object.assign({}, action.payload) }),
  [GET_SINGLE_EXERCISE]: (state, action) => (
    {
      ...state,
      currentExercise: Object.assign({}, action.payload),
      tests: Object.assign([], action.payload.views.map((object) =>
        Object.assign(
          {},
          object,
          { image: replaceWithBigger(object.image) }
        )
      )),
      backupTests: Object.assign([], action.payload.views.map((object) =>
        Object.assign(
          {},
          object,
          { image: replaceWithBigger(object.image) }
        )
      ))
    }
  ),
  [GET_TEMPLATE_THREE_EXERCISE]: (state, action) => (
    {
      ...state,
      currentExercise: Object.assign({}, action.payload),
      tests: Object.assign([], action.payload.views.map((object) =>
        Object.assign(
          {},
          object,
          {
            image: JSON.parse(object.image).map((el) => {
              if (el.length) {
                return Object.assign(JSON.parse(el), { key: shortid.generate() });
              }
              return Object.assign(el, { key: shortid.generate() });
            }),
            audio: JSON.parse(object.audio).map((el) => {
              if (el.length) {
                return Object.assign(JSON.parse(el), { key: shortid.generate() });
              }
              return Object.assign(el, { key: shortid.generate() });
            }),
            actions: JSON.parse(object.actions)
          }
        )
      )),
      backupTests: Object.assign([], action.payload.views.map((object) =>
        Object.assign(
          {},
          object,
          {
            image: JSON.parse(object.image).map((el) => (
              el.length ? JSON.parse(el) : el
            )),
            audio: JSON.parse(object.audio).map((el) => (
              el.length ? JSON.parse(el) : el
            )),
            actions: JSON.parse(object.actions)
          }
        )
      ))
    }
  ),
  [GET_TEMPLATE_ONE_EXERCISE]: (state, action) => (
    {
      ...state,
      currentExercise: Object.assign({}, action.payload),
      currentDescription: Object.assign({}, action.payload.settings),
      tests: Object.assign([], action.payload.views.map((object) =>
        Object.assign(
          {},
          object,
          {
            image: replaceWithBigger(JSON.parse(object.image).url),
            audio: JSON.parse(object.audio).url || getAudioUrl(object.audio),
            audioTitle: JSON.parse(object.audio).title
          }
        )
      )),
      backupTests: Object.assign([], action.payload.views.map((object) =>
        Object.assign(
          {},
          object,
          {
            image: replaceWithBigger(JSON.parse(object.image).url),
            audio: JSON.parse(object.audio).url || getAudioUrl(object.audio),
            audioTitle: JSON.parse(object.audio).title
          }
        )
      ))
    }
  ),
  [GET_TEMPLATE_NINE_EXERCISE]: (state, action) => (
    {
      ...state,
      currentExercise: Object.assign({}, action.payload),
      tests: Object.assign([], action.payload.views.map((object) =>
        Object.assign(
          {},
          object,
          { answers: object.incorrectAnswerPairs.map((obj) => obj.match) },
          { references: object.incorrectAnswerPairs.map((obj) => obj.word) }
        )
      ))
    }
  ),
  [TEMPLATES_GET_SKIPPED_WORDS_OPTIONS]: (state, action) => (
    {
      ...state,
      templateWordLists: action.payload
    }
  ),
  [UPDATE_SETTINGS]: (state, action) => (
    {
      ...state,
      currentSettings: action.payload
    }
  ),
  [UPDATE_TESTS]: (state, action) => (
    {
      ...state,
      tests: action.payload
    }
  ),
  [RESET_CURRENT_EXERCISE]: (state) => (
    {
      ...state,
      currentExercise: [],
      currentSettings: [],
      selectedTopic: '',
      topics: '',
      tests: [],
      lastExerciseId: state.currentExercise._id
    }
  ),
  [RESET_LAST_EXERCISE]: (state) => (
    {
      ...state,
      lastExerciseId: '',
      lastExerciseDuration: 0,
      lastExerciseResult: INITIAL_STATE.lastExerciseResult
    }
  ),
  [RESET_EXERCISES]: (state) => (
    {
      ...state,
      sections: [],
      tests: [],
      currentExercises: [],
      currentFavourites: []
    }
  ),
  [SHOW_OVERLAY]: (state, action) => (
    {
      ...state,
      overlay: action.payload
    }
  ),
  [EXERCISE_DURATION]: (state, action) => (
    {
      ...state,
      lastExerciseDuration: state.lastExerciseDuration + action.payload
    }
  ),
  [EXERCISE_RESULT]: (state, action) => (
    {
      ...state,
      lastExerciseResult: {
        total: state.lastExerciseResult.total + 1,
        correct: state.lastExerciseResult.correct + action.payload
      }
    }
  )
}, INITIAL_STATE);
