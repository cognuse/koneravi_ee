import React, { Component } from 'react';
import PropTypes from 'prop-types';

//redux
import { connect } from 'react-redux';
import {
  getExercise,
  resetCurrentExercise,
  showOverlay,
  getOptions,
  updateCurrentSettings,
  saveResults
} from '../state/actions';

//components
import ExerciseEight from '../components/ExerciseEight';
import Spinner from '../../common/components/spinner';

//utils, data
import { mergeSettingsOptions, transformSpecSymb } from '../helpers';
import { exerciseEightSettings } from '../data';
import { getQuery, saveResultsQuery } from '../../common/queries';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../common/dictionary/global';


class ExerciseEightView extends Component {
  static propTypes = {
    getExercise: PropTypes.func.isRequired,
    resetCurrentExercise: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      currentTest: 0
    };
    this.showNextTest = this.showNextTest.bind(this);
    this.triggerShowNextTest = this.triggerShowNextTest.bind(this);
  }

  componentWillMount() {
    const { locale, selectedTopic, getOptions, getExercise, updateCurrentSettings, match: { params: { id } } } = this.props;

    getOptions(locale)
      .then(() => getExercise(getQuery(id, 'exercise8'), getQuery(id, 'exercise8Views', selectedTopic)))
      .then(exercise => {
        const payload = mergeSettingsOptions(exercise.settings, exerciseEightSettings[locale]);
        updateCurrentSettings(payload);
      });
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
    this.startTime = new Date().getTime();
  }

  componentWillUnmount() {
    this.props.resetCurrentExercise();
  }

  showNextTest() {
    const { id, patientId, saveResults, history: { push }, showOverlay, tests } = this.props;
    const results = {
      query: saveResultsQuery,
      variables: {
        input: {
          created: new Date().toISOString(),
          exercise: id,
          patient: patientId,
          timeTotal: new Date().getTime() - this.startTime
        }
      }
    };
    saveResults(results);
    showOverlay(false);
    if (this.state.currentTest < tests.length - 1) {
      this.setState({ currentTest: ++this.state.currentTest });
      this.startExercise();
      this.startTime = new Date().getTime();
    } else {
      push('/thankyou');
    }
  }

  triggerShowNextTest(answer) {
    this.props.showOverlay();
    this.showNextTest(answer);
  }

  lightButton(id) {
    const currentButton = document.getElementById(id);
    currentButton.className += ' mdl-button--accent';
  }

  dimButton(id) {
    const prevButton = document.getElementById(id);
    prevButton.className = prevButton.className.replace('mdl-button--accent', '');
  }

  playAudio(id) {
    const currentAudio = document.getElementById(id);
    currentAudio.play();
  }

  playNext(i = 0) {
    const { tests, wordLists, intl: { formatMessage } } = this.props;
    const currentTest = tests[this.state.currentTest];
    const testQuestions = wordLists[transformSpecSymb(currentTest.topic.toLowerCase())];

    const startButton = document.getElementById('start-exercise-button');
    startButton.setAttribute('disabled', '');

    if (i < testQuestions.length) {
      if (i > 0) {
        this.dimButton(`exercise-${testQuestions[i - 1].text.toLowerCase()}-button`);
      }

      this.lightButton(`exercise-${testQuestions[i].text.toLowerCase()}-button`);

      if (currentTest.audio.toLowerCase() === formatMessage(globalMessages.off).toLowerCase()) {
        setTimeout(this.playNext.bind(this, i + 1), 3000);
      } else if (currentTest.skippedWords.some((word) => testQuestions[i].text === word)) {
        setTimeout(this.playNext.bind(this, i + 1), 3000);
      } else if (currentTest.audio.toLowerCase() === formatMessage(globalMessages.on).toLowerCase()) {
        this.playAudio(`exercise-${testQuestions[i].text.toLowerCase()}-audio`);
      } else {
        this.playAudio(`exercise-${testQuestions[i].text.toLowerCase()}-audio`);
      }
    } else {
      this.dimButton(`exercise-${testQuestions[i - 1].text.toLowerCase()}-button`);
      this.triggerShowNextTest(true);
    }
  }

  startExercise() {
    this.playNext();
  }

  render() {
    const { tests, settings, updateCurrentSettings, instructions, wordLists, history: { push } } = this.props;
    const currentTest = tests[this.state.currentTest];

    return tests.length && settings.length ?
      <ExerciseEight
        questionTitle={instructions}
        testQuestions={wordLists[transformSpecSymb(currentTest.topic.toLowerCase())]}
        questionImage={currentTest.image}
        originalText={currentTest.originalText}
        startExercise={this.startExercise.bind(this)}
        playNext={this.playNext.bind(this)}
        settings={settings}
        updateCurrentSettings={updateCurrentSettings}
        push={push}
      /> :
      <Spinner />;
  }
}

const mapStateToProps = (state) => ({
  wordLists: state.Exercises.templateWordLists,
  instructions: state.Exercises.currentSettings.shortCommand,
  settings: state.Exercises.currentSettings,
  tests: state.Exercises.tests,
  selectedTopic: state.Exercises.selectedTopic,
  locale: state.User.currentUser.locale,
  patientId: state.Patient.patientId,
  id: state.Exercises.currentExercise._id
});

export default connect(mapStateToProps, {
  getExercise,
  updateCurrentSettings,
  resetCurrentExercise,
  showOverlay,
  getOptions,
  saveResults
})(injectIntl(ExerciseEightView));
