import React, { Component } from 'react';
import PropTypes from 'prop-types';

//redux
import { connect } from 'react-redux';
import { getExercise, resetCurrentExercise, showOverlay, updateCurrentSettings, saveResults } from '../state/actions';
import { getQuery, saveResultsQuery } from '../../common/queries';

//utils data
import { shuffleArray, mergeSettingsOptions } from '../helpers';
import { exerciseFiveASettings } from '../data';

//components
import ExerciseFive from '../components/ExerciseFive/';
import Spinner from '../../common/components/spinner';


class ExerciseFiveBView extends Component {
  static propTypes = {
    getExercise: PropTypes.func.isRequired,
    resetCurrentExercise: PropTypes.func.isRequired,
    showOverlay: PropTypes.func.isRequired,
    updateCurrentSettings: PropTypes.func.isRequired,
    tests: PropTypes.array.isRequired,
    settings: PropTypes.array.isRequired,
    instructions: PropTypes.string
  };
  static defaultProps = {
    instructions: ''
  };

  constructor(props) {
    super(props);
    this.state = {
      currentTest: 0,
      correct: true
    };
    this.showNextTest = this.showNextTest.bind(this);
    this.triggerShowNextTest = this.triggerShowNextTest.bind(this);
  }

  componentWillMount() {
    const { locale, selectedTopic, getExercise, updateCurrentSettings, match: { params: { id } } } = this.props;

    getExercise(getQuery(id, 'exercise5a'), getQuery(id, 'exercise5AViews', selectedTopic))
      .then(exercise => {
        const payload = mergeSettingsOptions(exercise.settings, exerciseFiveASettings[locale]);
        updateCurrentSettings(payload);
      });
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
    this.startTime = new Date().getTime();
  }

  shouldComponentUpdate(nextProps, nextState) {
    //skip render on state.correct change
    return nextState.correct === this.state.correct;
  }

  componentWillUnmount() {
    this.props.resetCurrentExercise();
  }

  showNextTest() {
    const { patientId, id, history: { push }, showOverlay, tests, saveResults } = this.props;
    const { correct } = this.state;

    const results = {
      query: saveResultsQuery,
      variables: {
        input: {
          correct,
          created: new Date().toISOString(),
          exercise: id,
          patient: patientId,
          timeTotal: new Date().getTime() - this.startTime
        }
      }
    };
    saveResults(results);
    showOverlay(false);
    if (this.state.currentTest < tests.length - 1) {
      // setState should be separate to work correctly with shouldComponentUpdate condition
      this.setState({ currentTest: ++this.state.currentTest });
      this.setState({ correct: true });
      this.startTime = new Date().getTime();
    } else {
      push('/thankyou');
    }
  }

  triggerShowNextTest(answer) {
    if (answer) {
      this.props.showOverlay();
      this.showNextTest();
    } else if (!answer && this.state.correct) {
      this.setState({ correct: answer });
    }
  }

  formatAnswers(currentTest) {
    const { value: numberOfAnswers } = this.props.settings[4];
    const { correctAnswer } = currentTest;
    const modifiedArray = Object.assign([], currentTest.incorrectAnswers);
    if (Number.isInteger(numberOfAnswers) && modifiedArray.length >= numberOfAnswers) {
      modifiedArray.length = numberOfAnswers - 1;
    }
    modifiedArray.push(correctAnswer);
    return {
      correctAnswer,
      testQuestions: shuffleArray(modifiedArray)
    };
  }

  render() {
    const {
      tests,
      instructions,
      settings,
      updateCurrentSettings,
      history: { push }
    } = this.props;
    const currentTest = tests[this.state.currentTest];

    return tests.length && settings.length ?
      <ExerciseFive
        questionTitle={instructions}
        questionImage={currentTest.image}
        questionText={currentTest.sentence}
        testQuestions={this.formatAnswers(currentTest)}
        showNextTest={this.triggerShowNextTest}
        settings={settings}
        updateCurrentSettings={updateCurrentSettings}
        push={push}
        template="5b"
      />
      :
      <Spinner />;
  }
}

const mapStateToProps = (state) => ({
  instructions: state.Exercises.currentSettings.shortCommand,
  settings: state.Exercises.currentSettings,
  tests: state.Exercises.tests,
  selectedTopic: state.Exercises.selectedTopic,
  locale: state.User.currentUser.locale,
  patientId: state.Patient.patientId,
  id: state.Exercises.currentExercise._id
});

export default connect(mapStateToProps, {
  getExercise,
  updateCurrentSettings,
  resetCurrentExercise,
  showOverlay,
  saveResults
})(ExerciseFiveBView);
