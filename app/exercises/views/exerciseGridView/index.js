import React, { Component } from 'react';
import PropTypes from 'prop-types';

//redux
import { connect } from 'react-redux';
import {
  getExercises,
  setSections,
  modifySections,
  modifyExercises,
  modifyContent,
  modifyAccess,
  setFilters,
  reportResults,
  resetExercises,
  resetLastExerciseData
} from '../../state/actions';
import { logout } from '../../../user/state/actions';

//components
import HeaderTemplate from '../../../common/components/HeaderTemplate';
import TopBar from '../../../common/components/TopBar';
import Sidebar from '../../../common/components/Sidebar';
import ExercisesContent from '../../components/ExerciseContent';
import Spinner from '../../../common/components/spinner';
import NoExercises from '../../../common/components/NoExercises'

//data, utils
import { exerciseContentSections, filterSettings } from '../../data';
import { getQuery } from '../../../common/queries';

//styles
import styles from './exerciseGridView.css';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';
import { exercisesGridViewMessages } from '../../../common/dictionary/specific';

class ExerciseGridView extends Component {
  static propTypes = {
    exercises: PropTypes.array.isRequired,
    sections: PropTypes.array.isRequired,
    filters: PropTypes.object.isRequired,
    modifyExercises: PropTypes.func.isRequired,
    modifySections: PropTypes.func.isRequired,
    getExercises: PropTypes.func.isRequired,
    setSections: PropTypes.func.isRequired,
  };

  componentWillMount() {
    const { setSections, setFilters, getExercises, locale } = this.props;
    //extract filter values into object
    const filterObj = filterSettings[locale]
      .reduce((result, el) => [...result, ...el.options.map(el => el.value)], [])
      .reduce((result, el) => Object.assign(result, { [el]: 0 }), {});

    setSections(exerciseContentSections[locale]);
    setFilters(filterObj);
    getExercises(getQuery(null, 'exerciseGrid'));
  }

  componentDidMount() {
    const { resetLastExerciseData } = this.props;
    resetLastExerciseData();
    window.componentHandler.upgradeDom();
  }

  componentWillUnmount() {
    this.props.resetExercises();
  }

  render() {
    const {
      contentType,
      exercises,
      favourites,
      sections,
      modifySections,
      modifyExercises,
      modifyContent,
      modifyAccess,
      filters,
      userRole,
      reportResults,
      userId,
      token,
      logout,
      locale,
      accessDefaultVal,
      history: { push },
      intl: { formatMessage }
    } = this.props;

    const exercisesContent = contentType === 'favourites' ? favourites : exercises;

    let content;
    if (exercisesContent[0] === 'noExercises') {
      content = (
        <NoExercises>
          {formatMessage(globalMessages.noExercises)}
        </NoExercises>
      );
    } else if (exercisesContent.length || (contentType === 'favourites') || exercisesContent[0] === 'error') {
      content = <ExercisesContent exercises={exercisesContent} sections={sections} push={push} />;
    } else {
      content = <Spinner />;
    }

    let topBarSubtitle = '';
    if (contentType === 'all exercises' && accessDefaultVal === 'mine') {
      topBarSubtitle = formatMessage(exercisesGridViewMessages.exercisesGridViewTopSubtitle1);
    } else if (contentType === 'all exercises' && accessDefaultVal === 'others') {
      topBarSubtitle = formatMessage(exercisesGridViewMessages.exercisesGridViewTopSubtitle2);
    } else if (contentType === 'favourites' && (accessDefaultVal === 'mine' || accessDefaultVal === 'others')) {
      topBarSubtitle = formatMessage(exercisesGridViewMessages.exercisesGridViewTopSubtitle3);
    }

    return (
      <div>
        <div className={`mdl-layout mdl-js-layout mdl-layout--fixed-header ${styles.gridLayout}`}>
          <HeaderTemplate
            title={formatMessage(globalMessages.exercises)}
            userRole={userRole}
            reportResults={reportResults}
            userId={userId}
            token={token}
            logout={logout}
          />
          <div>
            <div className="mdl-grid mdl-grid--no-spacing" style={{ backgroundColor: '#EBEFF1' }}>
              <Sidebar
                filters={filters}
                modifySections={modifySections}
                modifyExercises={modifyExercises}
                modifyContent={modifyContent}
                modifyAccess={modifyAccess}
                locale={locale}
                userRole={userRole}
                accessDefaultVal={accessDefaultVal}
              />
              <div className="mdl-cell mdl-cell--10-col mdl-cell--6-col-tablet">
                <TopBar
                  title={formatMessage(globalMessages.exercises)}
                  subtitle={topBarSubtitle}
                />
                {content}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    exercises: state.Exercises.currentExercises,
    favourites: state.Exercises.currentFavourites,
    contentType: state.Exercises.contentType,
    sections: state.Exercises.sections,
    filters: state.Exercises.filters,
    accessDefaultVal: state.Exercises.accessType,
    userRole: state.User.currentUser.role,
    locale: state.User.currentUser.locale,
    userId: state.User.currentUser.id,
    token: state.User.currentUser.token
  };
}

export default connect(mapStateToProps, {
  getExercises,
  setSections,
  modifySections,
  modifyExercises,
  modifyContent,
  modifyAccess,
  setFilters,
  reportResults,
  resetExercises,
  logout,
  resetLastExerciseData
})(injectIntl(ExerciseGridView));
