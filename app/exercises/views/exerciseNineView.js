import React, { Component } from 'react';
import PropTypes from 'prop-types';

//redux
import { connect } from 'react-redux';
import { getExercise, resetCurrentExercise, updateCurrentSettings, saveResults } from '../state/actions';

//utils, data
import isEqual from 'lodash/isEqual';
import { mergeSettingsOptions, setExerciseDelay, shuffleArray } from '../helpers';
import { exerciseNineSettings } from '../data';
import { getQuery, saveResultsQuery } from '../../common/queries';

//components
import ExerciseNine from '../components/ExerciseNine/';
import Spinner from '../../common/components/spinner';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../common/dictionary/global';


class ExerciseNineView extends Component {
  static propTypes = {
    getExercise: PropTypes.func.isRequired,
    resetCurrentExercise: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      currentTest: 0,
      answersAmount: 0,
      currentAnswers: [null],
      currentResults: []
    };

    this.moveAnswer = this.moveAnswer.bind(this);
    this.showNextTest = this.showNextTest.bind(this);
    this.rearrange = this.rearrange.bind(this);
    this.backupState = this.backupState.bind(this);
    this.restoreState = this.restoreState.bind(this);
    this.setCurrentPairs = this.setCurrentPairs.bind(this);
  }

  componentWillMount() {
    const { locale, getExercise, updateCurrentSettings, match: { params: { id } } } = this.props;

    getExercise(getQuery(id, 'exercise9'), getQuery(id, 'exercise9Views'))
      .then(exercise => {
        const payload = mergeSettingsOptions(exercise.settings, exerciseNineSettings[locale]);
        updateCurrentSettings(payload);
      })
      .then(() => {
          const maxLength = this.props.settings[2].value;
          this.setCurrentPairs(maxLength);
        }
      );
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
    this.startTime = new Date().getTime();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.settings.length && this.props.settings.length && nextProps.settings[2].value !== this.props.settings[2].value) {
      const maxLength = nextProps.settings[2].value;
      this.setCurrentPairs(maxLength);
    }
  }

  componentDidUpdate() {
    const { formatMessage } = this.props.intl;
    const { tests } = this.props;
    const { currentTest } = this.state;
    const isValid = this.state.currentResults
      .every((el, index) => el && el.toLowerCase() === tests[currentTest].incorrectAnswerPairs[index].match.toLowerCase());
    if (this.state.currentAnswers.length === 0 && isValid) {
      const { settings } = this.props;
      const nextTestDelay = setExerciseDelay(settings[1].value);
      if (settings[4].value.toLowerCase() === formatMessage(globalMessages.yes).toLowerCase()) {
        setTimeout(() => {
          this.showNextTest(true);
        }, nextTestDelay);
      }
    }
  }

  componentWillUnmount() {
    this.props.resetCurrentExercise();
  }

  setCurrentPairs(maxLength) {
    const { tests } = this.props;
    const { currentTest } = this.state;

    const currentAnswers = Object.assign([], tests[currentTest].answers);
    currentAnswers.length = maxLength < currentAnswers.length ? maxLength : currentAnswers.length;
    const currentReferences = [];
    tests[currentTest].incorrectAnswerPairs.forEach((el) => {
      if (currentAnswers.indexOf(el.match) !== -1) {
        currentReferences.push(el.word);
      }
    });

    this.setState({
      currentAnswers: shuffleArray(currentAnswers),
      currentReferences,
      answersAmount: currentAnswers.length,
      currentResults: new Array(currentAnswers.length).fill(null)
    });
  }

  showNextTest(answer) {
    const { id, patientId, saveResults, history: { push }, tests, settings, intl: { formatMessage } } = this.props;
    const autoValidation = settings[4].value.toLowerCase() === formatMessage(globalMessages.yes).toLowerCase();

    const results = {
      query: saveResultsQuery,
      variables: {
        input: {
          created: new Date().toISOString(),
          exercise: id,
          patient: patientId,
          timeTotal: new Date().getTime() - this.startTime
        }
      }
    };
    if (!autoValidation) {
      results.variables.input.correct = answer;
    }
    saveResults(results);
    if (this.state.currentTest < tests.length - 1) {
      const maxLength = settings[2].value;
      const currentAnswers = Object.assign([], tests[this.state.currentTest + 1].answers);
      currentAnswers.length = maxLength < currentAnswers.length ? maxLength : currentAnswers.length;

      const currentReferences = [];
      tests[this.state.currentTest + 1].incorrectAnswerPairs.forEach((el) => {
        if (currentAnswers.indexOf(el.match) !== -1) {
          currentReferences.push(el.word);
        }
      });

      const answersAmount = currentAnswers.length;
      this.setState({
        answersAmount,
        currentAnswers: shuffleArray(currentAnswers),
        currentReferences,
        currentTest: ++this.state.currentTest,
        currentResults: new Array(currentAnswers.length).fill(null)
      });
      this.startTime = new Date().getTime();
    } else {
      push('/thankyou');
    }
  }

  backupState() {
    this.stateBackup = {};
    this.stateBackup.currentAnswers = Object.assign([], this.state.currentAnswers);
    this.stateBackup.currentResults = Object.assign([], this.state.currentResults);
  }

  restoreState() {
    const currentAnswers = Object.assign([], this.stateBackup.currentAnswers);
    const currentResults = Object.assign([], this.stateBackup.currentResults);
    if (!isEqual(currentAnswers, this.state.currentAnswers) || !isEqual(currentResults, this.state.currentResults)) {
      this.setState({ currentAnswers, currentResults });
    }
  }

  findEmpty(arr, index, answer) {
    let rightIndex;
    let leftIndex;
    for (let i = index + 1; i < arr.length; i++) {
      if (arr[i] === null || arr[i] === answer) {
        rightIndex = i;
        break;
      }
    }
    for (let i = index - 1; i >= 0; i--) {
      if (arr[i] === null || arr[i] === answer) {
        leftIndex = i;
        break;
      }
    }
    if (leftIndex === undefined) {
      return rightIndex;
    } else if (rightIndex === undefined) {
      return leftIndex;
    }
    return index - leftIndex >= rightIndex - index ? rightIndex : leftIndex;
  }

  rearrange(dropIndex, occupiedWith, { index, answer, inResult }) {
    const { currentResults } = this.state;
    if (occupiedWith && !inResult) {
      const emptyIndex = this.findEmpty(currentResults, dropIndex);
      if (dropIndex < emptyIndex) {
        currentResults.splice(dropIndex, 0, null);
        currentResults.splice(emptyIndex + 1, 1);
        this.setState({ currentResults });
      } else if (dropIndex > emptyIndex) {
        currentResults.splice(emptyIndex, 1);
        currentResults.splice(dropIndex, 0, null);
        this.setState({ currentResults });
      }
    } else if (occupiedWith && inResult) {
      const emptyIndex = this.findEmpty(currentResults, dropIndex, answer);
      if (dropIndex < emptyIndex) {
        currentResults.splice(dropIndex, 0, null);
        currentResults.splice(emptyIndex + 1, 1);
        this.setState({ currentResults });
      } else if (dropIndex > emptyIndex) {
        currentResults.splice(emptyIndex, 1);
        currentResults.splice(dropIndex, 0, null);
        this.setState({ currentResults });
      }
    }
  }

  moveAnswer(from, to, answer, inResult, occupiedWith) {
    const { currentAnswers, currentResults } = this.state;
    if (!inResult) {
      currentAnswers.splice(from, 1);
      currentResults[to] = answer;
      this.setState({ currentAnswers, currentResults });
    } else if (inResult && !occupiedWith) {
      if (currentResults[from] === answer) {
        currentResults.splice(from, 1, null);
      }
      currentResults[to] = answer;
      this.setState({ currentResults });
    }
  }

  render() {
    const { tests, instructions, settings, updateCurrentSettings, history: { push } } = this.props;
    const { currentAnswers, currentResults, answersAmount, currentReferences, currentTest } = this.state;
    return tests.length && settings.length && this.state.currentReferences ?
      <ExerciseNine
        answerPairs={tests[currentTest].incorrectAnswerPairs}
        questionTitle={instructions}
        currentAnswers={currentAnswers}
        currentResults={currentResults}
        correctAnswers={tests[currentTest].incorrectAnswerPairs}
        answersAmount={answersAmount}
        moveAnswer={this.moveAnswer}
        showNextTest={this.showNextTest}
        rearrange={this.rearrange}
        backupState={this.backupState}
        restoreState={this.restoreState}
        currentReferences={currentReferences}
        settings={settings}
        updateCurrentSettings={updateCurrentSettings}
        push={push}
      /> :
      <Spinner />;
  }
}

const mapStateToProps = (state) => ({
  instructions: state.Exercises.currentSettings.shortCommand,
  settings: state.Exercises.currentSettings,
  tests: state.Exercises.tests,
  locale: state.User.currentUser.locale,
  patientId: state.Patient.patientId,
  id: state.Exercises.currentExercise._id
});

export default connect(mapStateToProps, {
  getExercise,
  updateCurrentSettings,
  resetCurrentExercise,
  saveResults
})(injectIntl(ExerciseNineView));
