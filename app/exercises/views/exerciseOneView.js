import React, { Component } from 'react';
import PropTypes from 'prop-types';

//redux
import { connect } from 'react-redux';
import {
  getExercise,
  resetCurrentExercise,
  showOverlay,
  updateCurrentSettings,
  updateTests,
  uploadBlob,
  saveResults
} from '../state/actions';

//utils data
import { shuffleArray, mergeSettingsOptions } from '../helpers';
import { exerciseOneSettings } from '../data';
import { getQuery, saveResultsQuery } from '../../common/queries';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../common/dictionary/global';

//components
import ExerciseOne from '../components/ExerciseOne/index';
import Spinner from '../../common/components/spinner';


class ExerciseOneView extends Component {
  static propTypes = {
    getExercise: PropTypes.func.isRequired,
    resetCurrentExercise: PropTypes.func.isRequired,
    showOverlay: PropTypes.func.isRequired,
    updateCurrentSettings: PropTypes.func.isRequired,
    tests: PropTypes.array.isRequired,
    settings: PropTypes.array.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      currentTest: 0,
      audioFirst: false,
      correct: true
    };
    this.showNextTest = this.showNextTest.bind(this);
    this.triggerShowNextTest = this.triggerShowNextTest.bind(this);
  }

  componentWillMount() {
    const { locale, getExercise, updateCurrentSettings, selectedTopic, match: { params: { id } } } = this.props;

    getExercise(getQuery(id, 'exercise1'), getQuery(id, 'exercise1Views', selectedTopic))
      .then((exercise) => {
        const { settings } = exercise;
        const { updateTests, tests, showOverlay } = this.props;

        const payload = mergeSettingsOptions(settings, exerciseOneSettings[locale]);
        const audioFirst = id === '58b52de74f4c127ad2c21b2d';

        if (audioFirst) {
          this.setState({ audioFirst });
          showOverlay();
        }

        updateCurrentSettings(payload);
        if (settings.isRandom) {
          updateTests(shuffleArray(tests));
        }
      });
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
    this.startTime = new Date().getTime();
  }

  componentWillReceiveProps(nextProps) {
    const { formatMessage } = this.props.intl;
    const { updateTests, tests, backupTests } = this.props;
    if (this.props.settings.length && nextProps.settings[2].value !== this.props.settings[2].value) {
      if (nextProps.settings[2].value.toLowerCase() === formatMessage(globalMessages.yes).toLowerCase()) {
        updateTests(shuffleArray(tests));
      } else {
        updateTests(backupTests);
      }
      this.setState({ currentTest: 0 });
    }
  }

  componentWillUnmount() {
    this.props.resetCurrentExercise();
  }

  showNextTest(answer) {
    const { id, patientId, saveResults, history: { push }, showOverlay, tests } = this.props;
    const currentTest = tests[this.state.currentTest];
    const results = {
      query: saveResultsQuery,
      variables: {
        input: {
          created: new Date().toISOString(),
          exercise: id,
          patient: patientId,
          timeTotal: new Date().getTime() - this.startTime
        }
      }
    };
    if (currentTest.actions.toLowerCase() === 'correct & incorrect buttons') {
      results.variables.input.correct = answer;
    }
    saveResults(results);
    this.setState({ correct: true });
    if (!this.state.audioFirst) {
      showOverlay(false);
    }
    if (this.state.currentTest < tests.length - 1) {
      this.setState({ currentTest: ++this.state.currentTest });
      this.startTime = new Date().getTime();
    } else {
      push('/thankyou');
      showOverlay(false);
    }
  }

  triggerShowNextTest(answer) {
    // this.props.showOverlay();
    this.showNextTest(answer);
  }

  render() {
    const {
      id,
      tests,
      settings,
      updateCurrentSettings,
      currentDescription,
      uploadBlob,
      showOverlay,
      history: { push },
      intl: { formatMessage }
    } = this.props;
    const currentTest = tests[this.state.currentTest];

    return tests.length && settings.length && currentDescription ?
      <ExerciseOne
        audioTitle={currentTest.audioTitle}
        questionImage={currentTest.image}
        questionAudio={currentTest.audio}
        audioState={currentTest.audioState}
        actions={currentTest.actions}
        instructions={currentTest.originalText || currentDescription.shortCommand}
        showNextTest={this.triggerShowNextTest}
        settings={settings}
        updateCurrentSettings={updateCurrentSettings}
        uploadBlob={uploadBlob}
        showOverlay={showOverlay}
        push={push}
        audioFunctionality={tests.some(el => (
          el.actions.toLowerCase() === 'speech recognition' || el.actions === formatMessage(globalMessages.speechRecognition)
        ))}
        id={id}
      /> :
      <Spinner />;
  }
}

const mapStateToProps = (state) => ({
  settings: state.Exercises.currentSettings,
  tests: state.Exercises.tests,
  currentDescription: state.Exercises.currentDescription,
  backupTests: state.Exercises.backupTests,
  selectedTopic: state.Exercises.selectedTopic,
  locale: state.User.currentUser.locale,
  patientId: state.Patient.patientId,
  id: state.Exercises.currentExercise._id
});

export default connect(mapStateToProps, {
  getExercise,
  updateCurrentSettings,
  updateTests,
  resetCurrentExercise,
  showOverlay,
  uploadBlob,
  saveResults
})(injectIntl(ExerciseOneView));
