import React, { Component } from 'react';
import PropTypes from 'prop-types';

//redux
import { connect } from 'react-redux';
import {
  getExercise,
  resetCurrentExercise,
  updateCurrentSettings,
  showOverlay,
  updateTests,
  saveResults
} from '../state/actions';

//components
import ExerciseSevenA from '../components/ExerciseSevenA/';
import Spinner from '../../common/components/spinner';

//utils, data
import { mergeSettingsOptions, shuffleArray } from '../helpers';
import { exerciseSevenSettings } from '../data';
import { getQuery, saveResultsQuery } from '../../common/queries';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../common/dictionary/global';

class ExerciseSevenAView extends Component {
  static propTypes = {
    getExercise: PropTypes.func.isRequired,
    resetCurrentExercise: PropTypes.func.isRequired,
    updateCurrentSettings: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      currentTest: 0
    };
    this.showNextTest = this.showNextTest.bind(this);
  }

  componentWillMount() {
    const { locale, getExercise, updateCurrentSettings, selectedTopic,  match: { params: { id } } } = this.props;

    getExercise(getQuery(id, 'exercise7a'), getQuery(id, 'exercise7aViews', selectedTopic))
      .then(exercise => {
        const { settings } = exercise;
        const { updateTests, tests } = this.props;
        const payload = mergeSettingsOptions(settings, exerciseSevenSettings[locale]);

        updateCurrentSettings(payload);
        if (settings.isRandom) {
          updateTests(shuffleArray(tests));
        }
      });
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
    this.startTime = new Date().getTime();
  }

  componentWillReceiveProps(nextProps) {
    const { updateTests, tests, backupTests, intl: { formatMessage } } = this.props;
    if (this.props.settings.length && nextProps.settings[1].value !== this.props.settings[1].value) {
      if (nextProps.settings[1].value.toLowerCase() === formatMessage(globalMessages.yes).toLowerCase()) {
        updateTests(shuffleArray(tests));
      } else {
        updateTests(backupTests);
      }
      this.setState({ currentTest: 0 });
    }
  }

  componentWillUnmount() {
    this.props.resetCurrentExercise();
  }

  showNextTest(answer) {
    const { id, patientId, saveResults, settings, history: { push }, showOverlay, tests, intl: { formatMessage } } = this.props;
    const autoValidation = settings[2].value.toLowerCase() === formatMessage(globalMessages.yes).toLowerCase();
    const results = {
      query: saveResultsQuery,
      variables: {
        input: {
          created: new Date().toISOString(),
          exercise: id,
          patient: patientId,
          timeTotal: new Date().getTime() - this.startTime
        }
      }
    };
    if (!autoValidation) {
      results.variables.input.correct = answer;
    }
    saveResults(results);
    showOverlay(false);
    if (this.state.currentTest < tests.length - 1) {
      this.setState({ currentTest: ++this.state.currentTest });
      this.startTime = new Date().getTime();
    } else {
      push('/thankyou');
    }
  }

  render() {
    const {
      tests,
      instructions,
      settings,
      updateCurrentSettings,
      showOverlay,
      history: { push }
    } = this.props;
    const currentTest = tests[this.state.currentTest];

    return tests.length && settings.length ?
      <ExerciseSevenA
        questionTitle={instructions}
        questionImage={currentTest.image}
        questionAudio={JSON.parse(currentTest.audio).url}
        correctAnswer={currentTest.correctAnswer}
        showNextTest={this.showNextTest}
        handleResultValue={this.handleResultValue}
        settings={settings}
        showOverlay={showOverlay}
        updateCurrentSettings={updateCurrentSettings}
        push={push}
      />
      :
      <Spinner />;
  }
}

function mapStateToProps(state) {
  return {
    instructions: state.Exercises.currentSettings.shortCommand,
    settings: state.Exercises.currentSettings,
    tests: state.Exercises.tests,
    backupTests: state.Exercises.backupTests,
    selectedTopic: state.Exercises.selectedTopic,
    locale: state.User.currentUser.locale,
    patientId: state.Patient.patientId,
    id: state.Exercises.currentExercise._id
  };
}

export default connect(mapStateToProps, {
  getExercise,
  resetCurrentExercise,
  updateCurrentSettings,
  showOverlay,
  updateTests,
  saveResults
})(injectIntl(ExerciseSevenAView));
