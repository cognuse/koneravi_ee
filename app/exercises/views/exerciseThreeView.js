import React, { Component } from 'react';
import PropTypes from 'prop-types';

//redux
import { connect } from 'react-redux';
import {
  getExercise,
  resetCurrentExercise,
  showOverlay,
  updateCurrentSettings,
  updateTests,
  saveResults,
  resetLastExerciseData
} from '../state/actions';

//utils, data
import { shuffleArray, mergeSettingsOptions } from '../helpers';
import { exerciseThreeSettings } from '../data';
import { getQuery, saveResultsQuery } from '../../common/queries';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../common/dictionary/global';

//components
import ExerciseThree from '../components/ExerciseThree';
import Spinner from '../../common/components/spinner';


class ExerciseThreeView extends Component {
  static propTypes = {
    getExercise: PropTypes.func.isRequired,
    resetCurrentExercise: PropTypes.func.isRequired,
    showOverlay: PropTypes.func.isRequired,
    updateCurrentSettings: PropTypes.func.isRequired,
    tests: PropTypes.array.isRequired,
    settings: PropTypes.array.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      currentTest: 0,
      correct: true
    };
    this.showNextTest = this.showNextTest.bind(this);
    this.triggerShowNextTest = this.triggerShowNextTest.bind(this);
    this.showImages = this.showImages.bind(this);
  }

  componentWillMount() {
    const {
      locale,
      selectedTopic,
      getExercise,
      match: { params: { id } },
      updateCurrentSettings,
      resetLastExerciseData
    } = this.props;

    resetLastExerciseData();
    getExercise(getQuery(id, 'exercise3'), getQuery(id, 'exercise3Views', selectedTopic))
      .then(exercise => {
        const { updateTests, tests, showOverlay } = this.props;
        const { settings } = exercise;

        const payload = mergeSettingsOptions(settings, exerciseThreeSettings[locale]);
        updateCurrentSettings(payload);
        if (settings.isRandom) {
          updateTests(shuffleArray(tests));
        }
        if (settings.inverted) {
          // showOverlay();
        }
      });
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
    this.startTime = new Date().getTime();
  }

  componentWillReceiveProps(nextProps) {
    const { updateTests, tests, backupTests, intl: { formatMessage } } = this.props;
    if (this.props.settings.length && nextProps.settings[1].value !== this.props.settings[1].value) {
      if (nextProps.settings[1].value.toLowerCase() === formatMessage(globalMessages.yes).toLowerCase()) {
        updateTests(shuffleArray(tests));
      } else {
        updateTests(backupTests);
      }
      this.setState({ currentTest: 0 });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    //skip render on state.correct change
    return nextState.correct === this.state.correct;
  }

  componentWillUnmount() {
    const { resetCurrentExercise, showOverlay } = this.props;
    resetCurrentExercise();
    // showOverlay(false);
  }

  showNextTest(correct) {
    const {
      id,
      patientId,
      saveResults,
      settings,
      showOverlay,
      tests,
      history: { push },
      intl: { formatMessage }
    } = this.props;
    let { currentTest } = this.state;
    // const { correct } = this.state;
    const results = {
      query: saveResultsQuery,
      variables: {
        input: {
          correct,
          created: new Date().toISOString(),
          exercise: id,
          patient: patientId,
          timeTotal: new Date().getTime() - this.startTime
        }
      }
    };
    saveResults(results);
    if (settings[2].value.toLowerCase() !== formatMessage(globalMessages.audioFirst).toLowerCase()) {
      // showOverlay(false);
      this.startTime = new Date().getTime();
    }
    if (currentTest < tests.length - 1) {
      // setState should be separate to work correctly with shouldComponentUpdate condition
      this.setState({ currentTest: ++currentTest });
      this.setState({ correct: true });
    } else {
      push('/thankyou');
      // showOverlay(false);
    }
  }

  triggerShowNextTest(answer) {
    this.showNextTest(answer);

    // if (answer) {
    //   this.props.showOverlay();
    // } else if (!answer && this.state.correct) {
    //   this.setState({ correct: answer });
    // }
  }

  showImages() {
    // this.props.showOverlay(false);
  }

  render() {
    const { tests, settings, updateCurrentSettings, history: { push } } = this.props;
    const currentTest = tests[this.state.currentTest];

    return tests.length && settings.length ?
      <ExerciseThree
        questionImage={currentTest.image.filter(el => !!el.url)}
        questionAudio={currentTest.audio.filter(el => !!el.url)}
        actions={currentTest.actions}
        instructions={currentTest.originalText}
        showNextTest={this.triggerShowNextTest}
        settings={settings}
        updateCurrentSettings={updateCurrentSettings}
        testIndex={this.state.currentTest}
        showImages={this.showImages}
        push={push}
      />
      :
      <Spinner />;
  }
}

function mapStateToProps(state) {
  return {
    settings: state.Exercises.currentSettings,
    tests: state.Exercises.tests,
    backupTests: state.Exercises.backupTests,
    selectedTopic: state.Exercises.selectedTopic,
    locale: state.User.currentUser.locale,
    patientId: state.Patient.patientId,
    id: state.Exercises.currentExercise._id
  };
}

export default connect(mapStateToProps, {
  getExercise,
  updateCurrentSettings,
  updateTests,
  resetCurrentExercise,
  showOverlay,
  saveResults,
  resetLastExerciseData
})(injectIntl(ExerciseThreeView));
