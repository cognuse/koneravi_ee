//styles
import 'normalize.css';
import 'material-design-lite/material.css';
import 'material-design-lite/material.js';
import '../styles/styles.css';
//react
import React from 'react';
import ReactDOM from 'react-dom';
//redux
import { Provider } from 'react-redux';
import store from './configureStore';
//utils
import mixpanel from 'mixpanel-browser';
//vendor
import '../vendor/dictate/dictate';
import '../vendor/dictate/recorder';
import '../vendor/recorder/WebAudioRecorder';
//react intl
import { IntlProvider } from 'react-intl';
import { language, messages } from './configureLocale';
//components
import App from './App';

// REAL TOKEN
const MIXPANEL_TOKEN = '0b6bad797e6bf4118a689f0814913356';
mixpanel.init(MIXPANEL_TOKEN);

// added to global scope to use it in components
global.mixpanel = mixpanel;

// If browser doesn't support Intl (i.e. Safari)
// the intl polyfill and locale data are imported

if (!window.intl) {
  require.ensure([
    'intl',
    'intl/locale-data/jsonp/en.js',
    'intl/locale-data/jsonp/et.js'
  ], (require) => {
    require('intl');
    require('intl/locale-data/jsonp/en.js');
    require('intl/locale-data/jsonp/et.js');
    ReactDOM.render(
      <IntlProvider locale={language} messages={messages}>
        <Provider store={store}>
          <App />
        </Provider>
      </IntlProvider>
      , document.querySelector('.app'));
  });
} else {
  ReactDOM.render(
    <IntlProvider locale={language} messages={messages}>
      <Provider store={store}>
        <App />
      </Provider>
    </IntlProvider>
    , document.querySelector('.app'));
}
