import React, { Component } from 'react';
import PropTypes from 'prop-types';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';

//styles
import styles from './newPatient.css';


class NewPatient extends Component {
  static propTypes = {
    createPatient: PropTypes.func.isRequired,
    solo: PropTypes.bool
  };
  static defaultProps = {
    solo: false
  };
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: ''
    };

    this.handlePatientCreation = this.handlePatientCreation.bind(this);
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
  }

  handlePatientCreation() {
    const { firstName, lastName } = this.state;
    const { createPatient, push } = this.props;
    createPatient(firstName, lastName);
    push('/practice');
  }

  render() {
    const { firstName, lastName } = this.state;
    const { solo, intl: { formatMessage } } = this.props;

    return (
      <div className={`${solo ? 'mdl-shadow--8dp' : ''} ${styles.container}`}>
        <div className={styles.header}>
          <span>{formatMessage(globalMessages.createPatient)}</span>
        </div>
        <div className={styles.content}>
          <div className={styles.input}>
            <div className="mdl-textfield mdl-js-textfield mdl-textfield--full-width">
              <input
                className="mdl-textfield__input"
                value={firstName}
                onChange={(e) => {
                  this.setState({ firstName: e.target.value });
                }}
              />
              <label className="mdl-textfield__label">{formatMessage(globalMessages.enterFirstName)}</label>
            </div>
            <div className="mdl-textfield mdl-js-textfield mdl-textfield--full-width">
              <input
                className="mdl-textfield__input"
                value={lastName}
                onChange={(e) => {
                  this.setState({ lastName: e.target.value });
                }}
              />
              <label className="mdl-textfield__label">{formatMessage(globalMessages.enterLastName)}</label>
            </div>
          </div>
          <div className={styles.button}>
            <button
              className="mdl-button mdl-js-button mdl-button--raised mdl-button--colored"
              onClick={this.handlePatientCreation}
            >
              {formatMessage(globalMessages.createPatientButton)}
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default injectIntl(NewPatient);
