import React, { Component } from 'react';
import PropTypes from 'prop-types';

//i18n declaration
import { injectIntl } from 'react-intl';
import { globalMessages } from '../../../common/dictionary/global';

//utils, data
import debounce from 'lodash/debounce';
import sortBy from 'lodash/sortBy';
import { Scrollbars } from 'react-custom-scrollbars';

//styles
import styles from './selectPatient.css';


class SelectPatient extends Component {
  static propTypes = {
    setPatient: PropTypes.func.isRequired,
    patients: PropTypes.array.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      patients: []
    };

    this.handleSelection = this.handleSelection.bind(this);
    this.renderPatients = this.renderPatients.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
  }

  componentWillMount() {
    const { patients } = this.props;
    const sortedPatients = sortBy(patients, 'firstname');
    this.setState({ patients: sortedPatients });
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
  }

  handleSelection(id) {
    const { push, setPatient } = this.props;
    setPatient(id);
    push('/practice');
  }

  handleSearchChange(value) {
    const { patients } = this.props;
    const filteredPatients = patients.filter(({ firstname, lastname }) => (
      `${firstname.toLowerCase()}${lastname.toLowerCase()}`.search(value) > -1)
    );
    const sortedPatients = sortBy(filteredPatients, 'firstname');
    this.setState({ patients: sortedPatients });
  }

  renderPatients({ lastname, firstname, _id }) {
    return (
      <div
        key={_id}
        className={styles.row}
        onClick={() => {
          this.handleSelection(_id);
        }}
      >
        <i className="material-icons">person</i>
        {`${firstname ? firstname : ''} ${lastname ? lastname : ''}`}
      </div>
    );
  }

  render() {
    const { patients } = this.state;
    const { formatMessage } = this.props.intl;
    const debounced = debounce((value) => { this.handleSearchChange(value); }, 500);

    return (
      <div className={styles.container}>
        <div className={styles.header}>
          <span>{formatMessage(globalMessages.selectPatient)}</span>
          <div className={`mdl-textfield mdl-js-textfield ${styles.searchInputWrapper}`}>
            <i className={`material-icons ${styles.searchIcon}`}>search</i>
            <input
              className={`mdl-textfield__input ${styles.searchInput}`}
              type="text"
              id="image-picker-input"
              onChange={(e) => debounced(e.target.value)}
            />
          </div>
        </div>
        <div className={styles.content}>
          <Scrollbars>
            {patients.map(this.renderPatients)}
          </Scrollbars>
        </div>
      </div>
    );
  }
}

export default injectIntl(SelectPatient);
