import axios from 'axios';
import { PATIENTS_URL, apiConfig } from '../../common/api';
import { PATIENT_SET_PATIENTS, PATIENT_SET_PATIENT } from './constants';


export function getPatients() {
  return (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    axios.get(PATIENTS_URL, apiConfig(token))
      .then(result => {
        const ids = result.data.map(({ _id }) => _id);
        return axios.post(`${PATIENTS_URL}/batch`, ids, apiConfig(token));
      })
      .then(result => {
        dispatch({
          type: PATIENT_SET_PATIENTS,
          payload: result.data.items
        });
      })
      .catch(err => {
        console.error(err);
      });
  };
}

export function createPatient(firstName, lastName) {
  return (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    if (!firstName || !lastName) {
      console.log('enter first and last name');
      return;
    }
    const payload = {
      firstname: firstName,
      lastname: lastName
    };
    axios.post(PATIENTS_URL, payload, apiConfig(token))
      .then(result => {
        dispatch(setPatient(result.data._id));
      })
      .catch(err => {
        console.error(err);
      });
  };
}

export function setPatient(payload) {
  return {
    type: PATIENT_SET_PATIENT,
    payload
  };
}
