import { PATIENT_SET_PATIENTS, PATIENT_SET_PATIENT } from './constants';

const INITIAL_STATE = {
  patients: [],
  patientId: '',
  patientsReceived: false
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case PATIENT_SET_PATIENTS:
      return {
        ...state,
        patients: action.payload,
        patientsReceived: true
      };
    case PATIENT_SET_PATIENT:
      return {
        ...state,
        patients: [],
        patientsReceived: false,
        patientId: action.payload
      };
    default:
      return state;
  }
}
