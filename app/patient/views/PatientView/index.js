import React, { Component } from 'react';

//redux
import { connect } from 'react-redux';
import { getPatients, setPatient, createPatient } from '../../state/actions';

//components
import SelectPatient from '../../components/SelectPatient/index';
import NewPatient from '../../components/NewPatient/index';
import Spinner from '../../../common/components/spinner';

//styles
import styles from './patientView.css';


class PatientsView extends Component {
  componentWillMount() {
    this.props.getPatients();
  }

  componentDidMount() {
    window.componentHandler.upgradeDom();
  }

  render() {
    const { loaded, patients, setPatient, createPatient, push } = this.props;

    if (!loaded) {
      return (
        <div className={`mdl-shadow--8dp ${styles.patientsBlock}`}>
          <Spinner />
        </div>
      );
    }

    return patients.length ?
      <div className={`mdl-shadow--8dp ${styles.patientsBlock}`}>
        <div className={styles.leftSection}>
          <SelectPatient patients={patients} setPatient={setPatient} push={push} />
        </div>
        <div className={styles.rightSection}>
          <NewPatient createPatient={createPatient} push={push} />
        </div>
      </div>
      :
      <div className={styles.createPatient}>
        <NewPatient createPatient={createPatient} push={push} solo />
      </div>;
  }
}

function mapStateToProps(state) {
  return {
    patients: state.Patient.patients,
    loaded: state.Patient.patientsReceived
  };
}

export default connect(mapStateToProps, {
  getPatients,
  setPatient,
  createPatient
})(PatientsView);
