import React, { Component } from 'react';
//router
import { Route } from 'react-router-dom';
//auth
import { AuthWrapper } from './user/containers/AuthWrapper';
//components
import LoginRoute from './user/containers/loginRoute';
import ExerciseWrapper from './exercises/components/exerciseWrapper';
import Landing from './common/views/Landing';
import ThankYouPage from './exercises/views/ThankYouPage';
import { ExerciseGridView } from './exercises/views/index';

class Routes extends Component {
  render() {
    return (
      <div>
        <Route exact path="/" component={AuthWrapper(Landing)} />
        <Route path="/login/:route" component={LoginRoute} />
        <Route path="/thankyou" component={AuthWrapper(ThankYouPage)} />
        <Route path="/practice" component={AuthWrapper(ExerciseGridView)} />
        <Route path="/exercises" component={AuthWrapper(ExerciseWrapper)} />
      </div>
    );
  }
}

export default Routes;
