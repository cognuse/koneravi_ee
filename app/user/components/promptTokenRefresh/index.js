import React, { Component } from 'react';

//redux
import { connect } from 'react-redux';
import { getTokenExpirationData, showPromptTokenRefresh, setCurrentUser, refreshUser, logout } from '../../state/actions';
import { USER_PROMPT_TIME } from '../../state/constants';
import { getMinutesFromSeconds } from '../../helpers';

//i18n declaration
import { injectIntl } from 'react-intl';
import { promptTokenRefresh } from '../../../common/dictionary/specific';

//styles
import styles from './promptTokenRefresh.css';


class PromptTokenRefresh extends Component {
  constructor(props) {
    super(props);

    this.state = {
      time: USER_PROMPT_TIME
    };

    this.continueAction = this.continueAction.bind(this);
    this.logOutAction = this.logOutAction.bind(this);
  }

  componentWillMount() {
    this.interval = setInterval(() => {
      this.setState({ time: this.state.time - 1 });
    }, 1000);
  }
  componentDidMount() {
    componentHandler.upgradeDom();
  }
  shouldComponentUpdate(nextProps, nextState) {
    return (this.state.time !== nextState.time);
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }
  continueAction() {
    this.props.refreshUser();
  }
  logOutAction() {
    mixpanel.reset();
    this.props.logout();
  }
  render() {
    const { showPromptTokenRefresh, intl: { formatMessage } } = this.props;
    if (this.state.time < 0) {
      this.logOutAction();
    }

    return (
      <div className="mdl-shadow--4dp">
        <div className={styles.shade} onClick={() => showPromptTokenRefresh(false)} />
        <div className={styles.promptTokenRefresh}>
          <div className={styles.description}>
            <p>{formatMessage(promptTokenRefresh.description)}</p>
          </div>
          <div className={styles.time}>
            <h2>{getMinutesFromSeconds(this.state.time)}</h2>
          </div>
          <div className={styles.buttons}>
            <div className={styles.buttonsLeft}>
              <button
                className="mdl-button mdl-js-button mdl-button--raised mdl-color--green-500"
                onClick={this.continueAction}
              >
                {formatMessage(promptTokenRefresh.continue)}
              </button>
            </div>
            <div className={styles.buttonsRight}>
              <button
                className="mdl-button mdl-js-button mdl-button--raised"
                onClick={this.logOutAction}
              >
                {formatMessage(promptTokenRefresh.logOut)}
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    expiration: state.User.expiration
  };
}

export default connect(mapStateToProps, {
  getTokenExpirationData,
  showPromptTokenRefresh,
  setCurrentUser,
  refreshUser,
  logout
})(injectIntl(PromptTokenRefresh));
