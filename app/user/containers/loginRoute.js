import React, { Component } from 'react';
import PropTypes from 'prop-types';
//redux
import { connect } from 'react-redux';
import { setCurrentUser, getTokenExpirationData, logout } from '../state/actions';
import { decodeToken } from '../helpers';
//component
import Spinner from '../../common/components/spinner';


class LoginRoute extends Component {
  static propTypes = {
    setCurrentUser: PropTypes.func.isRequired,
    getTokenExpirationData: PropTypes.func.isRequired,
    logout: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
  };

  componentWillMount() {
    const {
      setCurrentUser,
      getTokenExpirationData,
      logout,
      history: { push },
      match: { params: { route: token } }
    } = this.props;

    if (token.split('.').length === 3) {
      setCurrentUser(decodeToken(token));
      push('/');
    } else {
      logout();
    }
    getTokenExpirationData();
  }

  render() {
    return (
      <Spinner />
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.User.currentUser
});

export default connect(mapStateToProps, {
  setCurrentUser,
  getTokenExpirationData,
  logout
})(LoginRoute);
