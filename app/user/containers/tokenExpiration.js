import React, { PureComponent } from 'react';

//redux
import { connect } from 'react-redux';
import { getTokenExpirationData } from '../state/actions';

//components
import PromptTokenRefresh from '../components/promptTokenRefresh/index';


class TokenExpiration extends PureComponent {
  componentDidMount() {
    this.props.getTokenExpirationData();
  }

  render() {
    if (!this.props.showPromptTokenRefresh) {
      return null;
    }
    return <PromptTokenRefresh />;
  }
}

const mapStateToProps = (state) => ({
  showPromptTokenRefresh: state.User.showPromptTokenRefresh
});

export default connect(mapStateToProps, {
  getTokenExpirationData
})(TokenExpiration);
