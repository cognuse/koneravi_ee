//utils
import base64 from 'base-64';

export function getMinutesFromSeconds(time) {
  const minutes = String(Math.floor(time / 60));
  let seconds = String(time % 60);
  if (seconds < 10) {
    seconds = `0${seconds}`;
  }
  return `${minutes}:${seconds}`;
}

export function redirectToLogin(url) {
  const ORIGIN = window.location.origin;
  const ENV = 'production';
  const READY_ORIGIN = encodeURIComponent(ORIGIN).split('%3A').join(':');
  window.location.href = `${url}?url=${READY_ORIGIN}&env=${ENV}`;
}

export function decodeToken(token) {
  const userElements = token.split('.');
  const user = base64.decode(userElements[1]);
  return Object.assign({}, JSON.parse(user).claim, { token });
}
