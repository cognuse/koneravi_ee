import axios from 'axios';
import { redirectToLogin } from '../helpers';
import {
  EXPIRATION_URL,
  REFRESH_URL,
  LOGIN_URL
} from '../../common/api';

//constants
import {
  USER_SET_CURRENT_USER,
  USER_GET_TOKEN_EXPIRATION_DATA,
  USER_REFRESH_USER,
  USER_LOGOUT,
  USER_SHOW_PROMPT_TOKEN_REFRESH,
  USER_PROMPT_TIME
} from './constants';

export function setCurrentUser(object) {
  return {
    payload: object,
    type: USER_SET_CURRENT_USER
  };
}

let showPromptTimeout;
let logoutTimeout;
const setPromptTokenRefreshTimer = (time, dispatch) => {
  if (showPromptTimeout) {
    clearTimeout(showPromptTimeout);
  }
  if (logoutTimeout) {
    clearTimeout(logoutTimeout);
  }
  showPromptTimeout = setTimeout(() => {
    dispatch(showPromptTokenRefresh(true));
  }, (time - USER_PROMPT_TIME) * 1000);
  logoutTimeout = setTimeout(() => {
    logout();
  }, time * 1000);
};

export function getTokenExpirationData() {
  return (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    axios.post(EXPIRATION_URL, { token })
      .then(response => {
        const { ttl, result } = response.data;
        dispatch({
          type: USER_GET_TOKEN_EXPIRATION_DATA,
          payload: {
            result,
            ttl
          }
        });
        setPromptTokenRefreshTimer(ttl, dispatch);
      })
      .catch(err => {
        console.error(err);
      });
  };
}

export function showPromptTokenRefresh(bool) {
  return {
    payload: bool,
    type: USER_SHOW_PROMPT_TOKEN_REFRESH
  };
}

export function refreshUser() {
  return (dispatch, getState) => {
    const token = getState().User.currentUser.token;
    axios.post(REFRESH_URL, { token })
      .then(result => {
        console.log('result 1', result);
        dispatch({
          payload: result.data.token,
          type: USER_REFRESH_USER
        });
        dispatch(showPromptTokenRefresh(false));
      })
      .then(() => {
        getTokenExpirationData()(dispatch, getState);
      })
      .catch(err => {
        console.error(err);
      });
  };
}

export function logout() {
  redirectToLogin(LOGIN_URL);
  return {
    type: USER_LOGOUT
  };
}
