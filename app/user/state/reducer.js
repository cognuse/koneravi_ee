import { decodeToken } from '../helpers';
import {
  USER_SET_CURRENT_USER,
  USER_GET_TOKEN_EXPIRATION_DATA,
  USER_REFRESH_USER,
  USER_LOGOUT,
  USER_SHOW_PROMPT_TOKEN_REFRESH
} from './constants';

export const INITIAL_STATE = {
  currentUser: {},
  expiration: undefined,
  authenticating: true,
  isAuthenticated: false,
  showPromptTokenRefresh: false
};

export default function (state = INITIAL_STATE, action) {
  let currentUser;
  switch (action.type) {
    case USER_SET_CURRENT_USER:
      mixpanel.identify(action.payload.id);
      currentUser = action.payload;
      if (currentUser.locale === 'ee') {
        currentUser.locale = 'et';
      }
      return {
        ...state,
        currentUser
      };
    case USER_GET_TOKEN_EXPIRATION_DATA:
      return {
        ...state,
        authenticating: false,
        expiration: action.payload.ttl,
        isAuthenticated: action.payload.result
      };
    case USER_SHOW_PROMPT_TOKEN_REFRESH:
      return {
        ...state,
        showPromptTokenRefresh: action.payload
      };
    case USER_REFRESH_USER:
      currentUser = decodeToken(action.payload);
      if (currentUser.locale === 'ee') {
        currentUser.locale = 'et';
      }
      mixpanel.identify(currentUser.id);
      return {
        ...state,
        currentUser
      };
    case USER_LOGOUT:
      return INITIAL_STATE;
    default:
      return state;
  }
}
