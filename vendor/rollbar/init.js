import Rollbar from 'rollbar';

// Rollbar
let rollbar;
if (process.env.NODE_ENV === 'production') {
  const rollbarConfig = {
    accessToken: process.env.ROLLBAR_TOKEN,
    captureUncaught: true,
    captureUnhandledRejections: true,
    payload: { environment: process.env.NODE_ENV }
  };

  rollbar = new Rollbar(rollbarConfig);
  console.log('Rollbar activated');
} else {
  rollbar = {
    error(msg, e) { console.error(msg, e); },
    info(msg, e) { console.warn(msg, e); },
    log(msg, e) { console.log(msg, e); }
  };
}

export default rollbar;
